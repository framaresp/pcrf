import { OnInit, EventEmitter } from '@angular/core';
export declare class CpWithButtonComponent implements OnInit {
    postal_code: string;
    placeholder: string;
    button_clicked: EventEmitter<string>;
    text_button: string;
    province: string;
    disabled: boolean;
    constructor();
    ngOnInit(): void;
    postalCodeChecked(event: any): void;
    buttonCLicked(event: any): void;
}
