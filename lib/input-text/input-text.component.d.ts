import { OnInit, EventEmitter } from '@angular/core';
export declare class InputTextComponent implements OnInit {
    placeholder: string;
    value: string;
    valueChange: EventEmitter<String>;
    constructor();
    ngOnInit(): void;
    onDataChange(event: any): void;
}
