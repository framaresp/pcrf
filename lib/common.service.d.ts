import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
export declare class CommonService {
    private httpClient;
    httpOptions: {
        headers: HttpHeaders;
    };
    constructor(httpClient: HttpClient);
    checkPostalCode(token: string, postalCode: string): Observable<any>;
    login(): Observable<any>;
    initGaia(token: string): Observable<any>;
    getTridionData(page_id: string): Observable<any>;
    getTridionComponent(com_id: string): Observable<any>;
    getPublicationData(pub_id: any): Observable<any>;
    getSmartTarget(device: string): Observable<any>;
    createPageInTridion(publication_id: string, page_title: string, file_name: string, pink_folder_id: string, page_template_id: string): Observable<any>;
    createComponentToPageInTridion(yellow_folder_id: string, schema_id: string, title: string, content: any): Observable<any>;
    addComponentToPageInTridion(page_id: string, components: any): Observable<any>;
}
