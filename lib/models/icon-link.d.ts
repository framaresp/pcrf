export declare class IconLink {
    click: Function;
    icon: string;
    label: string;
}
