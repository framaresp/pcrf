import { OnInit } from '@angular/core';
import { IconLink } from '../models/icon-link';
export declare class FooterComponent implements OnInit {
    left_links: IconLink[];
    right_links: IconLink[];
    footer_text: string;
    constructor();
    ngOnInit(): void;
    openIconLinks(link: any, event: any): void;
}
