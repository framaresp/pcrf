import { IconLink } from './../models/icon-link';
import { OnInit } from '@angular/core';
export declare class HeaderComponent implements OnInit {
    logo_url: string;
    logo_title: string;
    loco_click: Function;
    title: string;
    subtitle: string;
    contact_phone: string;
    icon_links: IconLink[];
    constructor();
    ngOnInit(): void;
    loco_clicked(event: any): void;
    openIconLinks(link: any, event: any): void;
}
