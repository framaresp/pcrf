import { OnInit, EventEmitter } from '@angular/core';
export declare class RedButtonComponent implements OnInit {
    disabled: Boolean;
    event: EventEmitter<boolean>;
    text: string;
    constructor();
    ngOnInit(): void;
    action(event: any): void;
}
