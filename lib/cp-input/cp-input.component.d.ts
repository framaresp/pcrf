import { CommonService } from './../common.service';
import { OnInit, EventEmitter } from '@angular/core';
export declare class CpInputComponent implements OnInit {
    private commonService;
    postal_code: string;
    placeholder: string;
    postal_code_info: EventEmitter<any>;
    private token;
    constructor(commonService: CommonService);
    ngOnInit(): void;
    checkPostalCode(): void;
}
