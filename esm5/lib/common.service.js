/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
var CommonService = /** @class */ (function () {
    function CommonService(httpClient) {
        this.httpClient = httpClient;
        this.httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            })
        };
    }
    /**
     * @param {?} token
     * @param {?} postalCode
     * @return {?}
     */
    CommonService.prototype.checkPostalCode = /**
     * @param {?} token
     * @param {?} postalCode
     * @return {?}
     */
    function (token, postalCode) {
        return this.httpClient.get("https://apisb.mapfre.com/srv/pocTarificadorVidaComeRound/components/mfcDeyde/codigoPostal", {
            headers: new HttpHeaders({
                Authorization: "Bearer " + token
            }),
            params: new HttpParams().set('parameters', "{\"cp\":\"" + postalCode + "\",\"validatePostalCode\":false}"),
            withCredentials: true
        });
    };
    /**
     * @return {?}
     */
    CommonService.prototype.login = /**
     * @return {?}
     */
    function () {
        return this.httpClient.post('https://apisb.mapfre.com/srv/pocTarificadorVidaComeRound/login', {}, {
            headers: new HttpHeaders({
                Authorization: 'Basic QVJDVVNBSTpNYXBmcmUyMDE4'
            })
        });
    };
    /**
     * @param {?} token
     * @return {?}
     */
    CommonService.prototype.initGaia = /**
     * @param {?} token
     * @return {?}
     */
    function (token) {
        return this.httpClient.post('https://apisb.mapfre.com/srv/pocTarificadorVidaComeRound/register/globaldata', {
            data: {
                config: {
                    real: true,
                    product: 'Vida',
                    branch: 'Vida',
                    entity: 'Vida',
                    group: 'Vida',
                    geo: 'espaÃ±a',
                    core: 'Vida',
                    language: 'es-es',
                    currency: 'â¬',
                    dateFormat: '',
                    direction: false,
                    theme: 'Classic Left',
                    rv: '',
                    helpIcon: 'mfc-icon--info',
                    enterprise: 'Vida',
                    version: 'v4.12.3',
                    analytics: {
                        viewState: 'stepView',
                        viewShadowBox: 'popupView',
                        sendForm: 'formLink'
                    },
                    itemsPromotion: '1',
                    aggregators: ['confu'],
                    dataStackQuote: "lOJ14enR3T5G5/0EUXy2ke14T+vk99QWV7EBlPyt8RtWqLhJzcbouFNLMxxyjs81k89JHRGvMop2Y9SKjx1C5SLm0vzqc1cLboFEyse\n              'auBWwbHXrtQgftGU6I5O5aslWGDQaPKVzpQqCEmXoKfEPlCvZyiULbHuF26gYLo4LMhjpPYnCTZlv+aRsjde6/aNFjKciQxd4pQg=",
                    dataStackBuy: "8NRR4c21peCL37o6o8mBwMIrGj4FAYgFU3eceIKK97n7pIbxtqyMUO8AVC1j4e2/6jzZ2QK7vcUiNXIB/UTgl/hBfvHB+s53HZX7ua6M\n              JPe90o3N6cLEaU3nv/HjLvEXADbTE9OpyfoidoD97Hfta0zeSB0k3yhDI6E12oIsMO6J5rRtXQd5Rq6mVF18CFYSQCb8sZIClrxZxys4j\n              EI1OYTygUc+HUHWQpkN/5fGqIw5BImBH27ux9xXSfRXjuXzrQheTKx4qYQWhBaze6VqOnOrfJo6dmcPuU1Tsz7UpCzd1v5IiOa0T4YBbx\n              YypXhDzgm44cFJseQeMNYL/tT+5W57Gh/hx0le4mTS6wpzZhZ/CVdrZTsZCBkl7HNrlBss9LFCYufTG6nOlUH8p17Re/8xmZu0+8/3DSM\n              xoXEBBl2VFCRjpyfh8bdojaBSU/2LvUV8Xbc9e/q+9lyFmHq2soZTqMaWDuKa7gznO+JjBNzRS3/TUiWZUz8cgBbruTp5S1J89PbXhe0r\n              iPUYZ3jYxez0dUW+I3V9WVsS1vqXrl8J8cXSS0EKgkTR5hUgejSwBMtWQPD7R7FOSqzt5UIV9+rKVnFI33Mm7e/O/ex09HwtuU/3KmNBL\n              BsD9f/HCPtwiT+sSkA/Awfh6X2FNHC0KPVq9PNTTrgVkZ2iIY5DVa/khum8t0bGHYIxHJvXY+z+OUKKljwK5wnp0BMhzYRf47TGCA9w6q\n              gv1m5+Ep2q5w2da0oqy6Gbxp7yqV3MXbnqKIiFfAquryA=",
                    exitApp: {}
                },
                browser: {
                    platform: 'Windows',
                    navigator: {
                        navigatorType: 'Chrome',
                        navigatorVersion: '74.0.3729.108'
                    },
                    mobile: false
                }
            }
        }, {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                Authorization: "Bearer " + token
            }),
            withCredentials: true
        });
    };
    /**
     * @param {?} page_id
     * @return {?}
     */
    CommonService.prototype.getTridionData = /**
     * @param {?} page_id
     * @return {?}
     */
    function (page_id) {
        return this.httpClient.get("https://poc-api-mapfre.ciberexperis.net/api/PageContents/" + page_id, this.httpOptions);
    };
    /**
     * @param {?} com_id
     * @return {?}
     */
    CommonService.prototype.getTridionComponent = /**
     * @param {?} com_id
     * @return {?}
     */
    function (com_id) {
        com_id = com_id.replace('tcm:', '');
        return this.httpClient.get("https://poc-api-mapfre.ciberexperis.net/api/Components/" + com_id, this.httpOptions);
    };
    /**
     * @param {?} pub_id
     * @return {?}
     */
    CommonService.prototype.getPublicationData = /**
     * @param {?} pub_id
     * @return {?}
     */
    function (pub_id) {
        return this.httpClient.get("https://poc-api-mapfre.ciberexperis.net/api/PublicationPageContents/" + pub_id, this.httpOptions);
    };
    /**
     * @param {?} device
     * @return {?}
     */
    CommonService.prototype.getSmartTarget = /**
     * @param {?} device
     * @return {?}
     */
    function (device) {
        return this.httpClient.post('https://poc-smarttarget-mapfre.ciberexperis.net/xo-promotions/_search?pretty', {
            query: {
                bool: {
                    must: [
                        { match: { scopePublication: 'tcm:0-3-1' } },
                        {
                            match: {
                                'triggers.triggerValues.stringValues': device
                            }
                        },
                        { match: { 'triggers.triggerValues.longValues': 114 } },
                        { match: { state: 'PENDING_ACTIVATION' } }
                    ]
                }
            }
        }, this.httpOptions);
    };
    // Create content in tridion
    // Create content in tridion
    /**
     * @param {?} publication_id
     * @param {?} page_title
     * @param {?} file_name
     * @param {?} pink_folder_id
     * @param {?} page_template_id
     * @return {?}
     */
    CommonService.prototype.createPageInTridion = 
    // Create content in tridion
    /**
     * @param {?} publication_id
     * @param {?} page_title
     * @param {?} file_name
     * @param {?} pink_folder_id
     * @param {?} page_template_id
     * @return {?}
     */
    function (publication_id, page_title, file_name, pink_folder_id, page_template_id) {
        /** @type {?} */
        var body = {
            Id: 'tcm:0-0-0',
            Title: page_title,
            FileName: file_name,
            LocationInfo: {
                PublishLocationPath: '',
                PublishLocationUrl: '',
                PublishPath: '',
                ContextRepository: {
                    IdRef: publication_id,
                    Title: ''
                },
                OrganizationalItem: {
                    IdRef: pink_folder_id,
                    Title: ''
                },
                Path: '',
                WebDavUrl: ''
            },
            PageTemplate: {
                IdRef: page_template_id,
                Title: ''
            }
        };
        return this.httpClient.post('https://poc-api-mapfre.ciberexperis.net/api/Page', body, this.httpOptions);
    };
    /**
     * @param {?} yellow_folder_id
     * @param {?} schema_id
     * @param {?} title
     * @param {?} content
     * @return {?}
     */
    CommonService.prototype.createComponentToPageInTridion = /**
     * @param {?} yellow_folder_id
     * @param {?} schema_id
     * @param {?} title
     * @param {?} content
     * @return {?}
     */
    function (yellow_folder_id, schema_id, title, content) {
        /** @type {?} */
        var body = {
            Id: 'tcm:0-0-0',
            SchemaID: schema_id,
            Title: title,
            FolderID: yellow_folder_id,
            Content: content
        };
        return this.httpClient.post('https://poc-api-mapfre.ciberexperis.net/api/ComponentContents', body, this.httpOptions);
    };
    /**
     * @param {?} page_id
     * @param {?} components
     * @return {?}
     */
    CommonService.prototype.addComponentToPageInTridion = /**
     * @param {?} page_id
     * @param {?} components
     * @return {?}
     */
    function (page_id, components) {
        /** @type {?} */
        var body = {
            PageId: page_id,
            ComponentPresentations: components
        };
        return this.httpClient.put('https://poc-api-mapfre.ciberexperis.net/api/Page', body, this.httpOptions);
    };
    CommonService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    CommonService.ctorParameters = function () { return [
        { type: HttpClient }
    ]; };
    /** @nocollapse */ CommonService.ngInjectableDef = i0.defineInjectable({ factory: function CommonService_Factory() { return new CommonService(i0.inject(i1.HttpClient)); }, token: CommonService, providedIn: "root" });
    return CommonService;
}());
export { CommonService };
if (false) {
    /** @type {?} */
    CommonService.prototype.httpOptions;
    /**
     * @type {?}
     * @private
     */
    CommonService.prototype.httpClient;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tbW9uLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AcGNyZi9jb21tb24vIiwic291cmNlcyI6WyJsaWIvY29tbW9uLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLFVBQVUsRUFBRSxXQUFXLEVBQUUsVUFBVSxFQUFFLE1BQU0sc0JBQXNCLENBQUM7OztBQUczRTtJQVVFLHVCQUFvQixVQUFzQjtRQUF0QixlQUFVLEdBQVYsVUFBVSxDQUFZO1FBTjFDLGdCQUFXLEdBQUc7WUFDWixPQUFPLEVBQUUsSUFBSSxXQUFXLENBQUM7Z0JBQ3ZCLGNBQWMsRUFBRSxrQkFBa0I7YUFDbkMsQ0FBQztTQUNILENBQUM7SUFFMkMsQ0FBQzs7Ozs7O0lBRTlDLHVDQUFlOzs7OztJQUFmLFVBQWdCLEtBQWEsRUFBRSxVQUFrQjtRQUMvQyxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUN4QiwyRkFBMkYsRUFDM0Y7WUFDRSxPQUFPLEVBQUUsSUFBSSxXQUFXLENBQUM7Z0JBQ3ZCLGFBQWEsRUFBRSxZQUFVLEtBQU87YUFDakMsQ0FBQztZQUNGLE1BQU0sRUFBRSxJQUFJLFVBQVUsRUFBRSxDQUFDLEdBQUcsQ0FDMUIsWUFBWSxFQUNaLGVBQVUsVUFBVSxxQ0FBK0IsQ0FDcEQ7WUFDRCxlQUFlLEVBQUUsSUFBSTtTQUN0QixDQUNGLENBQUM7SUFDSixDQUFDOzs7O0lBRUQsNkJBQUs7OztJQUFMO1FBQ0UsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FDekIsZ0VBQWdFLEVBQ2hFLEVBQUUsRUFDRjtZQUNFLE9BQU8sRUFBRSxJQUFJLFdBQVcsQ0FBQztnQkFDdkIsYUFBYSxFQUFFLGdDQUFnQzthQUNoRCxDQUFDO1NBQ0gsQ0FDRixDQUFDO0lBQ0osQ0FBQzs7Ozs7SUFFRCxnQ0FBUTs7OztJQUFSLFVBQVMsS0FBYTtRQUNwQixPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUN6Qiw4RUFBOEUsRUFDOUU7WUFDRSxJQUFJLEVBQUU7Z0JBQ0osTUFBTSxFQUFFO29CQUNOLElBQUksRUFBRSxJQUFJO29CQUNWLE9BQU8sRUFBRSxNQUFNO29CQUNmLE1BQU0sRUFBRSxNQUFNO29CQUNkLE1BQU0sRUFBRSxNQUFNO29CQUNkLEtBQUssRUFBRSxNQUFNO29CQUNiLEdBQUcsRUFBRSxTQUFTO29CQUNkLElBQUksRUFBRSxNQUFNO29CQUNaLFFBQVEsRUFBRSxPQUFPO29CQUNqQixRQUFRLEVBQUUsS0FBSztvQkFDZixVQUFVLEVBQUUsRUFBRTtvQkFDZCxTQUFTLEVBQUUsS0FBSztvQkFDaEIsS0FBSyxFQUFFLGNBQWM7b0JBQ3JCLEVBQUUsRUFBRSxFQUFFO29CQUNOLFFBQVEsRUFBRSxnQkFBZ0I7b0JBQzFCLFVBQVUsRUFBRSxNQUFNO29CQUNsQixPQUFPLEVBQUUsU0FBUztvQkFDbEIsU0FBUyxFQUFFO3dCQUNULFNBQVMsRUFBRSxVQUFVO3dCQUNyQixhQUFhLEVBQUUsV0FBVzt3QkFDMUIsUUFBUSxFQUFFLFVBQVU7cUJBQ3JCO29CQUNELGNBQWMsRUFBRSxHQUFHO29CQUNuQixXQUFXLEVBQUUsQ0FBQyxPQUFPLENBQUM7b0JBQ3RCLGNBQWMsRUFBRSwrTkFDeUY7b0JBQ3pHLFlBQVksRUFBRSw4M0JBT21DO29CQUNqRCxPQUFPLEVBQUUsRUFBRTtpQkFDWjtnQkFDRCxPQUFPLEVBQUU7b0JBQ1AsUUFBUSxFQUFFLFNBQVM7b0JBQ25CLFNBQVMsRUFBRTt3QkFDVCxhQUFhLEVBQUUsUUFBUTt3QkFDdkIsZ0JBQWdCLEVBQUUsZUFBZTtxQkFDbEM7b0JBQ0QsTUFBTSxFQUFFLEtBQUs7aUJBQ2Q7YUFDRjtTQUNGLEVBQ0Q7WUFDRSxPQUFPLEVBQUUsSUFBSSxXQUFXLENBQUM7Z0JBQ3ZCLGNBQWMsRUFBRSxrQkFBa0I7Z0JBQ2xDLGFBQWEsRUFBRSxZQUFVLEtBQU87YUFDakMsQ0FBQztZQUNGLGVBQWUsRUFBRSxJQUFJO1NBQ3RCLENBQ0YsQ0FBQztJQUNKLENBQUM7Ozs7O0lBRUQsc0NBQWM7Ozs7SUFBZCxVQUFlLE9BQWU7UUFDNUIsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FDeEIsOERBQTRELE9BQVMsRUFDckUsSUFBSSxDQUFDLFdBQVcsQ0FDakIsQ0FBQztJQUNKLENBQUM7Ozs7O0lBRUQsMkNBQW1COzs7O0lBQW5CLFVBQW9CLE1BQWM7UUFDaEMsTUFBTSxHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFLEVBQUUsQ0FBQyxDQUFDO1FBQ3BDLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQ3hCLDREQUEwRCxNQUFRLEVBQ2xFLElBQUksQ0FBQyxXQUFXLENBQ2pCLENBQUM7SUFDSixDQUFDOzs7OztJQUVELDBDQUFrQjs7OztJQUFsQixVQUFtQixNQUFNO1FBQ3ZCLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQ3hCLHlFQUF1RSxNQUFRLEVBQy9FLElBQUksQ0FBQyxXQUFXLENBQ2pCLENBQUM7SUFDSixDQUFDOzs7OztJQUVELHNDQUFjOzs7O0lBQWQsVUFBZSxNQUFjO1FBQzNCLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQ3pCLDhFQUE4RSxFQUM5RTtZQUNFLEtBQUssRUFBRTtnQkFDTCxJQUFJLEVBQUU7b0JBQ0osSUFBSSxFQUFFO3dCQUNKLEVBQUUsS0FBSyxFQUFFLEVBQUUsZ0JBQWdCLEVBQUUsV0FBVyxFQUFFLEVBQUU7d0JBQzVDOzRCQUNFLEtBQUssRUFBRTtnQ0FDTCxxQ0FBcUMsRUFBRSxNQUFNOzZCQUM5Qzt5QkFDRjt3QkFDRCxFQUFFLEtBQUssRUFBRSxFQUFFLG1DQUFtQyxFQUFFLEdBQUcsRUFBRSxFQUFFO3dCQUN2RCxFQUFFLEtBQUssRUFBRSxFQUFFLEtBQUssRUFBRSxvQkFBb0IsRUFBRSxFQUFFO3FCQUMzQztpQkFDRjthQUNGO1NBQ0YsRUFDRCxJQUFJLENBQUMsV0FBVyxDQUNqQixDQUFDO0lBQ0osQ0FBQztJQUVELDRCQUE0Qjs7Ozs7Ozs7OztJQUM1QiwyQ0FBbUI7Ozs7Ozs7Ozs7SUFBbkIsVUFDRSxjQUFzQixFQUN0QixVQUFrQixFQUNsQixTQUFpQixFQUNqQixjQUFzQixFQUN0QixnQkFBd0I7O1lBRWxCLElBQUksR0FBRztZQUNYLEVBQUUsRUFBRSxXQUFXO1lBQ2YsS0FBSyxFQUFFLFVBQVU7WUFDakIsUUFBUSxFQUFFLFNBQVM7WUFDbkIsWUFBWSxFQUFFO2dCQUNaLG1CQUFtQixFQUFFLEVBQUU7Z0JBQ3ZCLGtCQUFrQixFQUFFLEVBQUU7Z0JBQ3RCLFdBQVcsRUFBRSxFQUFFO2dCQUNmLGlCQUFpQixFQUFFO29CQUNqQixLQUFLLEVBQUUsY0FBYztvQkFDckIsS0FBSyxFQUFFLEVBQUU7aUJBQ1Y7Z0JBQ0Qsa0JBQWtCLEVBQUU7b0JBQ2xCLEtBQUssRUFBRSxjQUFjO29CQUNyQixLQUFLLEVBQUUsRUFBRTtpQkFDVjtnQkFDRCxJQUFJLEVBQUUsRUFBRTtnQkFDUixTQUFTLEVBQUUsRUFBRTthQUNkO1lBQ0QsWUFBWSxFQUFFO2dCQUNaLEtBQUssRUFBRSxnQkFBZ0I7Z0JBQ3ZCLEtBQUssRUFBRSxFQUFFO2FBQ1Y7U0FDRjtRQUVELE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQ3pCLGtEQUFrRCxFQUNsRCxJQUFJLEVBQ0osSUFBSSxDQUFDLFdBQVcsQ0FDakIsQ0FBQztJQUNKLENBQUM7Ozs7Ozs7O0lBRUQsc0RBQThCOzs7Ozs7O0lBQTlCLFVBQ0UsZ0JBQXdCLEVBQ3hCLFNBQWlCLEVBQ2pCLEtBQWEsRUFDYixPQUFZOztZQUVOLElBQUksR0FBRztZQUNYLEVBQUUsRUFBRSxXQUFXO1lBQ2YsUUFBUSxFQUFFLFNBQVM7WUFDbkIsS0FBSyxFQUFFLEtBQUs7WUFDWixRQUFRLEVBQUUsZ0JBQWdCO1lBQzFCLE9BQU8sRUFBRSxPQUFPO1NBQ2pCO1FBRUQsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FDekIsK0RBQStELEVBQy9ELElBQUksRUFDSixJQUFJLENBQUMsV0FBVyxDQUNqQixDQUFDO0lBQ0osQ0FBQzs7Ozs7O0lBRUQsbURBQTJCOzs7OztJQUEzQixVQUNFLE9BQWUsRUFDZixVQUFlOztZQUVULElBQUksR0FBRztZQUNYLE1BQU0sRUFBRSxPQUFPO1lBQ2Ysc0JBQXNCLEVBQUUsVUFBVTtTQUNuQztRQUVELE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQ3hCLGtEQUFrRCxFQUNsRCxJQUFJLEVBQ0osSUFBSSxDQUFDLFdBQVcsQ0FDakIsQ0FBQztJQUNKLENBQUM7O2dCQTdORixVQUFVLFNBQUM7b0JBQ1YsVUFBVSxFQUFFLE1BQU07aUJBQ25COzs7O2dCQUxRLFVBQVU7Ozt3QkFEbkI7Q0FrT0MsQUE5TkQsSUE4TkM7U0EzTlksYUFBYTs7O0lBQ3hCLG9DQUlFOzs7OztJQUVVLG1DQUE4QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEh0dHBDbGllbnQsIEh0dHBIZWFkZXJzLCBIdHRwUGFyYW1zIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xuXG5ASW5qZWN0YWJsZSh7XG4gIHByb3ZpZGVkSW46ICdyb290J1xufSlcbmV4cG9ydCBjbGFzcyBDb21tb25TZXJ2aWNlIHtcbiAgaHR0cE9wdGlvbnMgPSB7XG4gICAgaGVhZGVyczogbmV3IEh0dHBIZWFkZXJzKHtcbiAgICAgICdDb250ZW50LVR5cGUnOiAnYXBwbGljYXRpb24vanNvbidcbiAgICB9KVxuICB9O1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgaHR0cENsaWVudDogSHR0cENsaWVudCkge31cblxuICBjaGVja1Bvc3RhbENvZGUodG9rZW46IHN0cmluZywgcG9zdGFsQ29kZTogc3RyaW5nKTogT2JzZXJ2YWJsZTxhbnk+IHtcbiAgICByZXR1cm4gdGhpcy5odHRwQ2xpZW50LmdldChcbiAgICAgIGBodHRwczovL2FwaXNiLm1hcGZyZS5jb20vc3J2L3BvY1RhcmlmaWNhZG9yVmlkYUNvbWVSb3VuZC9jb21wb25lbnRzL21mY0RleWRlL2NvZGlnb1Bvc3RhbGAsXG4gICAgICB7XG4gICAgICAgIGhlYWRlcnM6IG5ldyBIdHRwSGVhZGVycyh7XG4gICAgICAgICAgQXV0aG9yaXphdGlvbjogYEJlYXJlciAke3Rva2VufWBcbiAgICAgICAgfSksXG4gICAgICAgIHBhcmFtczogbmV3IEh0dHBQYXJhbXMoKS5zZXQoXG4gICAgICAgICAgJ3BhcmFtZXRlcnMnLFxuICAgICAgICAgIGB7XCJjcFwiOlwiJHtwb3N0YWxDb2RlfVwiLFwidmFsaWRhdGVQb3N0YWxDb2RlXCI6ZmFsc2V9YFxuICAgICAgICApLFxuICAgICAgICB3aXRoQ3JlZGVudGlhbHM6IHRydWVcbiAgICAgIH1cbiAgICApO1xuICB9XG5cbiAgbG9naW4oKTogT2JzZXJ2YWJsZTxhbnk+IHtcbiAgICByZXR1cm4gdGhpcy5odHRwQ2xpZW50LnBvc3QoXG4gICAgICAnaHR0cHM6Ly9hcGlzYi5tYXBmcmUuY29tL3Nydi9wb2NUYXJpZmljYWRvclZpZGFDb21lUm91bmQvbG9naW4nLFxuICAgICAge30sXG4gICAgICB7XG4gICAgICAgIGhlYWRlcnM6IG5ldyBIdHRwSGVhZGVycyh7XG4gICAgICAgICAgQXV0aG9yaXphdGlvbjogJ0Jhc2ljIFFWSkRWVk5CU1RwTllYQm1jbVV5TURFNCdcbiAgICAgICAgfSlcbiAgICAgIH1cbiAgICApO1xuICB9XG5cbiAgaW5pdEdhaWEodG9rZW46IHN0cmluZyk6IE9ic2VydmFibGU8YW55PiB7XG4gICAgcmV0dXJuIHRoaXMuaHR0cENsaWVudC5wb3N0KFxuICAgICAgJ2h0dHBzOi8vYXBpc2IubWFwZnJlLmNvbS9zcnYvcG9jVGFyaWZpY2Fkb3JWaWRhQ29tZVJvdW5kL3JlZ2lzdGVyL2dsb2JhbGRhdGEnLFxuICAgICAge1xuICAgICAgICBkYXRhOiB7XG4gICAgICAgICAgY29uZmlnOiB7XG4gICAgICAgICAgICByZWFsOiB0cnVlLFxuICAgICAgICAgICAgcHJvZHVjdDogJ1ZpZGEnLFxuICAgICAgICAgICAgYnJhbmNoOiAnVmlkYScsXG4gICAgICAgICAgICBlbnRpdHk6ICdWaWRhJyxcbiAgICAgICAgICAgIGdyb3VwOiAnVmlkYScsXG4gICAgICAgICAgICBnZW86ICdlc3Bhw4PCsWEnLFxuICAgICAgICAgICAgY29yZTogJ1ZpZGEnLFxuICAgICAgICAgICAgbGFuZ3VhZ2U6ICdlcy1lcycsXG4gICAgICAgICAgICBjdXJyZW5jeTogJ8OiwoLCrCcsXG4gICAgICAgICAgICBkYXRlRm9ybWF0OiAnJyxcbiAgICAgICAgICAgIGRpcmVjdGlvbjogZmFsc2UsXG4gICAgICAgICAgICB0aGVtZTogJ0NsYXNzaWMgTGVmdCcsXG4gICAgICAgICAgICBydjogJycsXG4gICAgICAgICAgICBoZWxwSWNvbjogJ21mYy1pY29uLS1pbmZvJyxcbiAgICAgICAgICAgIGVudGVycHJpc2U6ICdWaWRhJyxcbiAgICAgICAgICAgIHZlcnNpb246ICd2NC4xMi4zJyxcbiAgICAgICAgICAgIGFuYWx5dGljczoge1xuICAgICAgICAgICAgICB2aWV3U3RhdGU6ICdzdGVwVmlldycsXG4gICAgICAgICAgICAgIHZpZXdTaGFkb3dCb3g6ICdwb3B1cFZpZXcnLFxuICAgICAgICAgICAgICBzZW5kRm9ybTogJ2Zvcm1MaW5rJ1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIGl0ZW1zUHJvbW90aW9uOiAnMScsXG4gICAgICAgICAgICBhZ2dyZWdhdG9yczogWydjb25mdSddLFxuICAgICAgICAgICAgZGF0YVN0YWNrUXVvdGU6IGBsT0oxNGVuUjNUNUc1LzBFVVh5MmtlMTRUK3ZrOTlRV1Y3RUJsUHl0OFJ0V3FMaEp6Y2JvdUZOTE14eHlqczgxazg5SkhSR3ZNb3AyWTlTS2p4MUM1U0xtMHZ6cWMxY0xib0ZFeXNlXG4gICAgICAgICAgICAgICdhdUJXd2JIWHJ0UWdmdEdVNkk1TzVhc2xXR0RRYVBLVnpwUXFDRW1Yb0tmRVBsQ3ZaeWlVTGJIdUYyNmdZTG80TE1oanBQWW5DVFpsdithUnNqZGU2L2FORmpLY2lReGQ0cFFnPWAsXG4gICAgICAgICAgICBkYXRhU3RhY2tCdXk6IGA4TlJSNGMyMXBlQ0wzN282bzhtQndNSXJHajRGQVlnRlUzZWNlSUtLOTduN3BJYnh0cXlNVU84QVZDMWo0ZTIvNmp6WjJRSzd2Y1VpTlhJQi9VVGdsL2hCZnZIQitzNTNIWlg3dWE2TVxuICAgICAgICAgICAgICBKUGU5MG8zTjZjTEVhVTNudi9Iakx2RVhBRGJURTlPcHlmb2lkb0Q5N0hmdGEwemVTQjBrM3loREk2RTEyb0lzTU82SjVyUnRYUWQ1UnE2bVZGMThDRllTUUNiOHNaSUNscnhaeHlzNGpcbiAgICAgICAgICAgICAgRUkxT1lUeWdVYytIVUhXUXBrTi81ZkdxSXc1QkltQkgyN3V4OXhYU2ZSWGp1WHpyUWhlVEt4NHFZUVdoQmF6ZTZWcU9uT3JmSm82ZG1jUHVVMVRzejdVcEN6ZDF2NUlpT2EwVDRZQmJ4XG4gICAgICAgICAgICAgIFl5cFhoRHpnbTQ0Y0ZKc2VRZU1OWUwvdFQrNVc1N0doL2h4MGxlNG1UUzZ3cHpaaFovQ1ZkclpUc1pDQmtsN0hOcmxCc3M5TEZDWXVmVEc2bk9sVUg4cDE3UmUvOHhtWnUwKzgvM0RTTVxuICAgICAgICAgICAgICB4b1hFQkJsMlZGQ1JqcHlmaDhiZG9qYUJTVS8yTHZVVjhYYmM5ZS9xKzlseUZtSHEyc29aVHFNYVdEdUthN2d6bk8rSmpCTnpSUzMvVFVpV1pVejhjZ0JicnVUcDVTMUo4OVBiWGhlMHJcbiAgICAgICAgICAgICAgaVBVWVozall4ZXowZFVXK0kzVjlXVnNTMXZxWHJsOEo4Y1hTUzBFS2drVFI1aFVnZWpTd0JNdFdRUEQ3UjdGT1NxenQ1VUlWOStyS1ZuRkkzM01tN2UvTy9leDA5SHd0dVUvM0ttTkJMXG4gICAgICAgICAgICAgIEJzRDlmL0hDUHR3aVQrc1NrQS9Bd2ZoNlgyRk5IQzBLUFZxOVBOVFRyZ1ZrWjJpSVk1RFZhL2todW04dDBiR0hZSXhISnZYWSt6K09VS0tsandLNXducDBCTWh6WVJmNDdUR0NBOXc2cVxuICAgICAgICAgICAgICBndjFtNStFcDJxNXcyZGEwb3F5NkdieHA3eXFWM01YYm5xS0lpRmZBcXVyeUE9YCxcbiAgICAgICAgICAgIGV4aXRBcHA6IHt9XG4gICAgICAgICAgfSxcbiAgICAgICAgICBicm93c2VyOiB7XG4gICAgICAgICAgICBwbGF0Zm9ybTogJ1dpbmRvd3MnLFxuICAgICAgICAgICAgbmF2aWdhdG9yOiB7XG4gICAgICAgICAgICAgIG5hdmlnYXRvclR5cGU6ICdDaHJvbWUnLFxuICAgICAgICAgICAgICBuYXZpZ2F0b3JWZXJzaW9uOiAnNzQuMC4zNzI5LjEwOCdcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBtb2JpbGU6IGZhbHNlXG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9LFxuICAgICAge1xuICAgICAgICBoZWFkZXJzOiBuZXcgSHR0cEhlYWRlcnMoe1xuICAgICAgICAgICdDb250ZW50LVR5cGUnOiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgICAgICAgQXV0aG9yaXphdGlvbjogYEJlYXJlciAke3Rva2VufWBcbiAgICAgICAgfSksXG4gICAgICAgIHdpdGhDcmVkZW50aWFsczogdHJ1ZVxuICAgICAgfVxuICAgICk7XG4gIH1cblxuICBnZXRUcmlkaW9uRGF0YShwYWdlX2lkOiBzdHJpbmcpOiBPYnNlcnZhYmxlPGFueT4ge1xuICAgIHJldHVybiB0aGlzLmh0dHBDbGllbnQuZ2V0KFxuICAgICAgYGh0dHBzOi8vcG9jLWFwaS1tYXBmcmUuY2liZXJleHBlcmlzLm5ldC9hcGkvUGFnZUNvbnRlbnRzLyR7cGFnZV9pZH1gLFxuICAgICAgdGhpcy5odHRwT3B0aW9uc1xuICAgICk7XG4gIH1cblxuICBnZXRUcmlkaW9uQ29tcG9uZW50KGNvbV9pZDogc3RyaW5nKTogT2JzZXJ2YWJsZTxhbnk+IHtcbiAgICBjb21faWQgPSBjb21faWQucmVwbGFjZSgndGNtOicsICcnKTtcbiAgICByZXR1cm4gdGhpcy5odHRwQ2xpZW50LmdldChcbiAgICAgIGBodHRwczovL3BvYy1hcGktbWFwZnJlLmNpYmVyZXhwZXJpcy5uZXQvYXBpL0NvbXBvbmVudHMvJHtjb21faWR9YCxcbiAgICAgIHRoaXMuaHR0cE9wdGlvbnNcbiAgICApO1xuICB9XG5cbiAgZ2V0UHVibGljYXRpb25EYXRhKHB1Yl9pZCk6IE9ic2VydmFibGU8YW55PiB7XG4gICAgcmV0dXJuIHRoaXMuaHR0cENsaWVudC5nZXQoXG4gICAgICBgaHR0cHM6Ly9wb2MtYXBpLW1hcGZyZS5jaWJlcmV4cGVyaXMubmV0L2FwaS9QdWJsaWNhdGlvblBhZ2VDb250ZW50cy8ke3B1Yl9pZH1gLFxuICAgICAgdGhpcy5odHRwT3B0aW9uc1xuICAgICk7XG4gIH1cblxuICBnZXRTbWFydFRhcmdldChkZXZpY2U6IHN0cmluZyk6IE9ic2VydmFibGU8YW55PiB7XG4gICAgcmV0dXJuIHRoaXMuaHR0cENsaWVudC5wb3N0KFxuICAgICAgJ2h0dHBzOi8vcG9jLXNtYXJ0dGFyZ2V0LW1hcGZyZS5jaWJlcmV4cGVyaXMubmV0L3hvLXByb21vdGlvbnMvX3NlYXJjaD9wcmV0dHknLFxuICAgICAge1xuICAgICAgICBxdWVyeToge1xuICAgICAgICAgIGJvb2w6IHtcbiAgICAgICAgICAgIG11c3Q6IFtcbiAgICAgICAgICAgICAgeyBtYXRjaDogeyBzY29wZVB1YmxpY2F0aW9uOiAndGNtOjAtMy0xJyB9IH0sXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBtYXRjaDoge1xuICAgICAgICAgICAgICAgICAgJ3RyaWdnZXJzLnRyaWdnZXJWYWx1ZXMuc3RyaW5nVmFsdWVzJzogZGV2aWNlXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICB7IG1hdGNoOiB7ICd0cmlnZ2Vycy50cmlnZ2VyVmFsdWVzLmxvbmdWYWx1ZXMnOiAxMTQgfSB9LFxuICAgICAgICAgICAgICB7IG1hdGNoOiB7IHN0YXRlOiAnUEVORElOR19BQ1RJVkFUSU9OJyB9IH1cbiAgICAgICAgICAgIF1cbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH0sXG4gICAgICB0aGlzLmh0dHBPcHRpb25zXG4gICAgKTtcbiAgfVxuXG4gIC8vIENyZWF0ZSBjb250ZW50IGluIHRyaWRpb25cbiAgY3JlYXRlUGFnZUluVHJpZGlvbihcbiAgICBwdWJsaWNhdGlvbl9pZDogc3RyaW5nLFxuICAgIHBhZ2VfdGl0bGU6IHN0cmluZyxcbiAgICBmaWxlX25hbWU6IHN0cmluZyxcbiAgICBwaW5rX2ZvbGRlcl9pZDogc3RyaW5nLFxuICAgIHBhZ2VfdGVtcGxhdGVfaWQ6IHN0cmluZ1xuICApOiBPYnNlcnZhYmxlPGFueT4ge1xuICAgIGNvbnN0IGJvZHkgPSB7XG4gICAgICBJZDogJ3RjbTowLTAtMCcsXG4gICAgICBUaXRsZTogcGFnZV90aXRsZSxcbiAgICAgIEZpbGVOYW1lOiBmaWxlX25hbWUsXG4gICAgICBMb2NhdGlvbkluZm86IHtcbiAgICAgICAgUHVibGlzaExvY2F0aW9uUGF0aDogJycsXG4gICAgICAgIFB1Ymxpc2hMb2NhdGlvblVybDogJycsXG4gICAgICAgIFB1Ymxpc2hQYXRoOiAnJyxcbiAgICAgICAgQ29udGV4dFJlcG9zaXRvcnk6IHtcbiAgICAgICAgICBJZFJlZjogcHVibGljYXRpb25faWQsXG4gICAgICAgICAgVGl0bGU6ICcnXG4gICAgICAgIH0sXG4gICAgICAgIE9yZ2FuaXphdGlvbmFsSXRlbToge1xuICAgICAgICAgIElkUmVmOiBwaW5rX2ZvbGRlcl9pZCxcbiAgICAgICAgICBUaXRsZTogJydcbiAgICAgICAgfSxcbiAgICAgICAgUGF0aDogJycsXG4gICAgICAgIFdlYkRhdlVybDogJydcbiAgICAgIH0sXG4gICAgICBQYWdlVGVtcGxhdGU6IHtcbiAgICAgICAgSWRSZWY6IHBhZ2VfdGVtcGxhdGVfaWQsXG4gICAgICAgIFRpdGxlOiAnJ1xuICAgICAgfVxuICAgIH07XG5cbiAgICByZXR1cm4gdGhpcy5odHRwQ2xpZW50LnBvc3QoXG4gICAgICAnaHR0cHM6Ly9wb2MtYXBpLW1hcGZyZS5jaWJlcmV4cGVyaXMubmV0L2FwaS9QYWdlJyxcbiAgICAgIGJvZHksXG4gICAgICB0aGlzLmh0dHBPcHRpb25zXG4gICAgKTtcbiAgfVxuXG4gIGNyZWF0ZUNvbXBvbmVudFRvUGFnZUluVHJpZGlvbihcbiAgICB5ZWxsb3dfZm9sZGVyX2lkOiBzdHJpbmcsXG4gICAgc2NoZW1hX2lkOiBzdHJpbmcsXG4gICAgdGl0bGU6IHN0cmluZyxcbiAgICBjb250ZW50OiBhbnlcbiAgKTogT2JzZXJ2YWJsZTxhbnk+IHtcbiAgICBjb25zdCBib2R5ID0ge1xuICAgICAgSWQ6ICd0Y206MC0wLTAnLFxuICAgICAgU2NoZW1hSUQ6IHNjaGVtYV9pZCxcbiAgICAgIFRpdGxlOiB0aXRsZSxcbiAgICAgIEZvbGRlcklEOiB5ZWxsb3dfZm9sZGVyX2lkLFxuICAgICAgQ29udGVudDogY29udGVudFxuICAgIH07XG5cbiAgICByZXR1cm4gdGhpcy5odHRwQ2xpZW50LnBvc3QoXG4gICAgICAnaHR0cHM6Ly9wb2MtYXBpLW1hcGZyZS5jaWJlcmV4cGVyaXMubmV0L2FwaS9Db21wb25lbnRDb250ZW50cycsXG4gICAgICBib2R5LFxuICAgICAgdGhpcy5odHRwT3B0aW9uc1xuICAgICk7XG4gIH1cblxuICBhZGRDb21wb25lbnRUb1BhZ2VJblRyaWRpb24oXG4gICAgcGFnZV9pZDogc3RyaW5nLFxuICAgIGNvbXBvbmVudHM6IGFueVxuICApOiBPYnNlcnZhYmxlPGFueT4ge1xuICAgIGNvbnN0IGJvZHkgPSB7XG4gICAgICBQYWdlSWQ6IHBhZ2VfaWQsXG4gICAgICBDb21wb25lbnRQcmVzZW50YXRpb25zOiBjb21wb25lbnRzXG4gICAgfTtcblxuICAgIHJldHVybiB0aGlzLmh0dHBDbGllbnQucHV0KFxuICAgICAgJ2h0dHBzOi8vcG9jLWFwaS1tYXBmcmUuY2liZXJleHBlcmlzLm5ldC9hcGkvUGFnZScsXG4gICAgICBib2R5LFxuICAgICAgdGhpcy5odHRwT3B0aW9uc1xuICAgICk7XG4gIH1cbn1cbiJdfQ==