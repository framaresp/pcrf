/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { RedButtonComponent } from './red-button/red-button.component';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { InputTextComponent } from './input-text/input-text.component';
import { ImageBoxComponent } from './image-box/image-box.component';
import { TextBoxComponent } from './text-box/text-box.component';
import { CpInputComponent } from './cp-input/cp-input.component';
import { CpWithButtonComponent } from './cp-with-button/cp-with-button.component';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
var PcrfCommonModule = /** @class */ (function () {
    function PcrfCommonModule() {
    }
    PcrfCommonModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [
                        HeaderComponent,
                        FooterComponent,
                        InputTextComponent,
                        ImageBoxComponent,
                        TextBoxComponent,
                        RedButtonComponent,
                        CpInputComponent,
                        CpWithButtonComponent
                    ],
                    imports: [
                        CommonModule,
                        MatButtonModule,
                        MatInputModule,
                        MatCardModule,
                        MatIconModule,
                        HttpClientModule
                    ],
                    exports: [
                        HeaderComponent,
                        FooterComponent,
                        InputTextComponent,
                        ImageBoxComponent,
                        TextBoxComponent,
                        RedButtonComponent,
                        CpInputComponent,
                        CpWithButtonComponent
                    ]
                },] }
    ];
    return PcrfCommonModule;
}());
export { PcrfCommonModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGNyZi1jb21tb24ubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQHBjcmYvY29tbW9uLyIsInNvdXJjZXMiOlsibGliL3BjcmYtY29tbW9uLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sbUNBQW1DLENBQUM7QUFDdkUsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUV6QyxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFDM0QsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLHlCQUF5QixDQUFDO0FBQ3pELE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUN2RCxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sd0JBQXdCLENBQUM7QUFFdkQsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBQzVELE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSwyQkFBMkIsQ0FBQztBQUM1RCxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxtQ0FBbUMsQ0FBQztBQUN2RSxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxpQ0FBaUMsQ0FBQztBQUNwRSxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQztBQUNqRSxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQztBQUNqRSxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSwyQ0FBMkMsQ0FBQztBQUNsRixPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUN4RCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFFL0M7SUFBQTtJQThCK0IsQ0FBQzs7Z0JBOUIvQixRQUFRLFNBQUM7b0JBQ1IsWUFBWSxFQUFFO3dCQUNaLGVBQWU7d0JBQ2YsZUFBZTt3QkFDZixrQkFBa0I7d0JBQ2xCLGlCQUFpQjt3QkFDakIsZ0JBQWdCO3dCQUNoQixrQkFBa0I7d0JBQ2xCLGdCQUFnQjt3QkFDaEIscUJBQXFCO3FCQUN0QjtvQkFDRCxPQUFPLEVBQUU7d0JBQ1AsWUFBWTt3QkFDWixlQUFlO3dCQUNmLGNBQWM7d0JBQ2QsYUFBYTt3QkFDYixhQUFhO3dCQUNiLGdCQUFnQjtxQkFDakI7b0JBQ0QsT0FBTyxFQUFFO3dCQUNQLGVBQWU7d0JBQ2YsZUFBZTt3QkFDZixrQkFBa0I7d0JBQ2xCLGlCQUFpQjt3QkFDakIsZ0JBQWdCO3dCQUNoQixrQkFBa0I7d0JBQ2xCLGdCQUFnQjt3QkFDaEIscUJBQXFCO3FCQUN0QjtpQkFDRjs7SUFDOEIsdUJBQUM7Q0FBQSxBQTlCaEMsSUE4QmdDO1NBQW5CLGdCQUFnQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFJlZEJ1dHRvbkNvbXBvbmVudCB9IGZyb20gJy4vcmVkLWJ1dHRvbi9yZWQtYnV0dG9uLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5pbXBvcnQgeyBNYXRCdXR0b25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9idXR0b24nO1xuaW1wb3J0IHsgTWF0SW5wdXRNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9pbnB1dCc7XG5pbXBvcnQgeyBNYXRDYXJkTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvY2FyZCc7XG5pbXBvcnQgeyBNYXRJY29uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvaWNvbic7XG5cbmltcG9ydCB7IEhlYWRlckNvbXBvbmVudCB9IGZyb20gJy4vaGVhZGVyL2hlYWRlci5jb21wb25lbnQnO1xuaW1wb3J0IHsgRm9vdGVyQ29tcG9uZW50IH0gZnJvbSAnLi9mb290ZXIvZm9vdGVyLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBJbnB1dFRleHRDb21wb25lbnQgfSBmcm9tICcuL2lucHV0LXRleHQvaW5wdXQtdGV4dC5jb21wb25lbnQnO1xuaW1wb3J0IHsgSW1hZ2VCb3hDb21wb25lbnQgfSBmcm9tICcuL2ltYWdlLWJveC9pbWFnZS1ib3guY29tcG9uZW50JztcbmltcG9ydCB7IFRleHRCb3hDb21wb25lbnQgfSBmcm9tICcuL3RleHQtYm94L3RleHQtYm94LmNvbXBvbmVudCc7XG5pbXBvcnQgeyBDcElucHV0Q29tcG9uZW50IH0gZnJvbSAnLi9jcC1pbnB1dC9jcC1pbnB1dC5jb21wb25lbnQnO1xuaW1wb3J0IHsgQ3BXaXRoQnV0dG9uQ29tcG9uZW50IH0gZnJvbSAnLi9jcC13aXRoLWJ1dHRvbi9jcC13aXRoLWJ1dHRvbi5jb21wb25lbnQnO1xuaW1wb3J0IHsgSHR0cENsaWVudE1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5cbkBOZ01vZHVsZSh7XG4gIGRlY2xhcmF0aW9uczogW1xuICAgIEhlYWRlckNvbXBvbmVudCxcbiAgICBGb290ZXJDb21wb25lbnQsXG4gICAgSW5wdXRUZXh0Q29tcG9uZW50LFxuICAgIEltYWdlQm94Q29tcG9uZW50LFxuICAgIFRleHRCb3hDb21wb25lbnQsXG4gICAgUmVkQnV0dG9uQ29tcG9uZW50LFxuICAgIENwSW5wdXRDb21wb25lbnQsXG4gICAgQ3BXaXRoQnV0dG9uQ29tcG9uZW50XG4gIF0sXG4gIGltcG9ydHM6IFtcbiAgICBDb21tb25Nb2R1bGUsXG4gICAgTWF0QnV0dG9uTW9kdWxlLFxuICAgIE1hdElucHV0TW9kdWxlLFxuICAgIE1hdENhcmRNb2R1bGUsXG4gICAgTWF0SWNvbk1vZHVsZSxcbiAgICBIdHRwQ2xpZW50TW9kdWxlXG4gIF0sXG4gIGV4cG9ydHM6IFtcbiAgICBIZWFkZXJDb21wb25lbnQsXG4gICAgRm9vdGVyQ29tcG9uZW50LFxuICAgIElucHV0VGV4dENvbXBvbmVudCxcbiAgICBJbWFnZUJveENvbXBvbmVudCxcbiAgICBUZXh0Qm94Q29tcG9uZW50LFxuICAgIFJlZEJ1dHRvbkNvbXBvbmVudCxcbiAgICBDcElucHV0Q29tcG9uZW50LFxuICAgIENwV2l0aEJ1dHRvbkNvbXBvbmVudFxuICBdXG59KVxuZXhwb3J0IGNsYXNzIFBjcmZDb21tb25Nb2R1bGUge31cbiJdfQ==