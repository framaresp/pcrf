/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input, Output, EventEmitter } from '@angular/core';
var CpWithButtonComponent = /** @class */ (function () {
    function CpWithButtonComponent() {
        this.button_clicked = new EventEmitter();
        this.disabled = true;
    }
    /**
     * @return {?}
     */
    CpWithButtonComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () { };
    /**
     * @param {?} event
     * @return {?}
     */
    CpWithButtonComponent.prototype.postalCodeChecked = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        console.log("postalCodeChecked: " + event);
        this.disabled = !event;
        this.province = event;
    };
    /**
     * @param {?} event
     * @return {?}
     */
    CpWithButtonComponent.prototype.buttonCLicked = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        console.log("buttonCLicked: " + event);
        this.button_clicked.emit(this.province);
    };
    CpWithButtonComponent.decorators = [
        { type: Component, args: [{
                    selector: 'pcrf-cp-with-button',
                    template: "<pcrf-cp-input\n  [postal_code]=\"postal_code\"\n  [placeholder]=\"placeholder\"\n  (postal_code_info)=\"postalCodeChecked($event)\"\n></pcrf-cp-input>\n\n<pcrf-red-button\n  [disabled]=\"disabled\"\n  (event)=\"buttonCLicked($event)\"\n  [text]=\"text_button\"\n></pcrf-red-button>\n",
                    styles: [""]
                }] }
    ];
    /** @nocollapse */
    CpWithButtonComponent.ctorParameters = function () { return []; };
    CpWithButtonComponent.propDecorators = {
        postal_code: [{ type: Input }],
        placeholder: [{ type: Input }],
        button_clicked: [{ type: Output }],
        text_button: [{ type: Input }]
    };
    return CpWithButtonComponent;
}());
export { CpWithButtonComponent };
if (false) {
    /** @type {?} */
    CpWithButtonComponent.prototype.postal_code;
    /** @type {?} */
    CpWithButtonComponent.prototype.placeholder;
    /** @type {?} */
    CpWithButtonComponent.prototype.button_clicked;
    /** @type {?} */
    CpWithButtonComponent.prototype.text_button;
    /** @type {?} */
    CpWithButtonComponent.prototype.province;
    /** @type {?} */
    CpWithButtonComponent.prototype.disabled;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3Atd2l0aC1idXR0b24uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQHBjcmYvY29tbW9uLyIsInNvdXJjZXMiOlsibGliL2NwLXdpdGgtYnV0dG9uL2NwLXdpdGgtYnV0dG9uLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxLQUFLLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUUvRTtJQWVFO1FBUFUsbUJBQWMsR0FBRyxJQUFJLFlBQVksRUFBVSxDQUFDO1FBS3RELGFBQVEsR0FBRyxJQUFJLENBQUM7SUFFRCxDQUFDOzs7O0lBRWhCLHdDQUFROzs7SUFBUixjQUFZLENBQUM7Ozs7O0lBRWIsaURBQWlCOzs7O0lBQWpCLFVBQWtCLEtBQUs7UUFDckIsT0FBTyxDQUFDLEdBQUcsQ0FBQyx3QkFBc0IsS0FBTyxDQUFDLENBQUM7UUFDM0MsSUFBSSxDQUFDLFFBQVEsR0FBRyxDQUFDLEtBQUssQ0FBQztRQUN2QixJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztJQUN4QixDQUFDOzs7OztJQUVELDZDQUFhOzs7O0lBQWIsVUFBYyxLQUFLO1FBQ2pCLE9BQU8sQ0FBQyxHQUFHLENBQUMsb0JBQWtCLEtBQU8sQ0FBQyxDQUFDO1FBQ3ZDLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUMxQyxDQUFDOztnQkE1QkYsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxxQkFBcUI7b0JBQy9CLHdTQUE4Qzs7aUJBRS9DOzs7Ozs4QkFFRSxLQUFLOzhCQUNMLEtBQUs7aUNBQ0wsTUFBTTs4QkFDTixLQUFLOztJQW9CUiw0QkFBQztDQUFBLEFBN0JELElBNkJDO1NBeEJZLHFCQUFxQjs7O0lBQ2hDLDRDQUE2Qjs7SUFDN0IsNENBQTZCOztJQUM3QiwrQ0FBc0Q7O0lBQ3RELDRDQUE2Qjs7SUFFN0IseUNBQWlCOztJQUVqQix5Q0FBZ0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQsIE91dHB1dCwgRXZlbnRFbWl0dGVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ3BjcmYtY3Atd2l0aC1idXR0b24nLFxuICB0ZW1wbGF0ZVVybDogJy4vY3Atd2l0aC1idXR0b24uY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9jcC13aXRoLWJ1dHRvbi5jb21wb25lbnQuc2NzcyddXG59KVxuZXhwb3J0IGNsYXNzIENwV2l0aEJ1dHRvbkNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gIEBJbnB1dCgpIHBvc3RhbF9jb2RlOiBzdHJpbmc7XG4gIEBJbnB1dCgpIHBsYWNlaG9sZGVyOiBzdHJpbmc7XG4gIEBPdXRwdXQoKSBidXR0b25fY2xpY2tlZCA9IG5ldyBFdmVudEVtaXR0ZXI8c3RyaW5nPigpO1xuICBASW5wdXQoKSB0ZXh0X2J1dHRvbjogc3RyaW5nO1xuXG4gIHByb3ZpbmNlOiBzdHJpbmc7XG5cbiAgZGlzYWJsZWQgPSB0cnVlO1xuXG4gIGNvbnN0cnVjdG9yKCkge31cblxuICBuZ09uSW5pdCgpIHt9XG5cbiAgcG9zdGFsQ29kZUNoZWNrZWQoZXZlbnQpIHtcbiAgICBjb25zb2xlLmxvZyhgcG9zdGFsQ29kZUNoZWNrZWQ6ICR7ZXZlbnR9YCk7XG4gICAgdGhpcy5kaXNhYmxlZCA9ICFldmVudDtcbiAgICB0aGlzLnByb3ZpbmNlID0gZXZlbnQ7XG4gIH1cblxuICBidXR0b25DTGlja2VkKGV2ZW50KSB7XG4gICAgY29uc29sZS5sb2coYGJ1dHRvbkNMaWNrZWQ6ICR7ZXZlbnR9YCk7XG4gICAgdGhpcy5idXR0b25fY2xpY2tlZC5lbWl0KHRoaXMucHJvdmluY2UpO1xuICB9XG59XG4iXX0=