/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { CommonService } from './../common.service';
import { Component, Input, Output, EventEmitter } from '@angular/core';
var CpInputComponent = /** @class */ (function () {
    function CpInputComponent(commonService) {
        this.commonService = commonService;
        this.postal_code_info = new EventEmitter();
    }
    /**
     * @return {?}
     */
    CpInputComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.commonService.login().subscribe((/**
         * @param {?} loginData
         * @return {?}
         */
        function (loginData) {
            _this.token = loginData.token;
            _this.commonService.initGaia(_this.token).subscribe((/**
             * @param {?} loaded
             * @return {?}
             */
            function (loaded) {
                console.log('Gaia init');
            }), (/**
             * @param {?} err
             * @return {?}
             */
            function (err) { return console.log(err); }));
        }), (/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return console.log(err); }));
    };
    /**
     * @return {?}
     */
    CpInputComponent.prototype.checkPostalCode = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.commonService.checkPostalCode(this.token, this.postal_code).subscribe((/**
         * @param {?} data
         * @return {?}
         */
        function (data) {
            _this.postal_code_info.emit(data.data);
        }), (/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return console.log(err); }));
    };
    CpInputComponent.decorators = [
        { type: Component, args: [{
                    selector: 'pcrf-cp-input',
                    template: "<pcrf-input-text\n  [placeholder]=\"placeholder\"\n  [(value)]=\"postal_code\"\n  (valueChange)=\"checkPostalCode()\"\n></pcrf-input-text>\n",
                    styles: [""]
                }] }
    ];
    /** @nocollapse */
    CpInputComponent.ctorParameters = function () { return [
        { type: CommonService }
    ]; };
    CpInputComponent.propDecorators = {
        postal_code: [{ type: Input }],
        placeholder: [{ type: Input }],
        postal_code_info: [{ type: Output }]
    };
    return CpInputComponent;
}());
export { CpInputComponent };
if (false) {
    /** @type {?} */
    CpInputComponent.prototype.postal_code;
    /** @type {?} */
    CpInputComponent.prototype.placeholder;
    /** @type {?} */
    CpInputComponent.prototype.postal_code_info;
    /**
     * @type {?}
     * @private
     */
    CpInputComponent.prototype.token;
    /**
     * @type {?}
     * @private
     */
    CpInputComponent.prototype.commonService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3AtaW5wdXQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQHBjcmYvY29tbW9uLyIsInNvdXJjZXMiOlsibGliL2NwLWlucHV0L2NwLWlucHV0LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBQ3BELE9BQU8sRUFBRSxTQUFTLEVBQVUsS0FBSyxFQUFFLE1BQU0sRUFBRSxZQUFZLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFL0U7SUFZRSwwQkFBb0IsYUFBNEI7UUFBNUIsa0JBQWEsR0FBYixhQUFhLENBQWU7UUFKdEMscUJBQWdCLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztJQUlGLENBQUM7Ozs7SUFFcEQsbUNBQVE7OztJQUFSO1FBQUEsaUJBYUM7UUFaQyxJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssRUFBRSxDQUFDLFNBQVM7Ozs7UUFDbEMsVUFBQSxTQUFTO1lBQ1AsS0FBSSxDQUFDLEtBQUssR0FBRyxTQUFTLENBQUMsS0FBSyxDQUFDO1lBQzdCLEtBQUksQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLEtBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxTQUFTOzs7O1lBQy9DLFVBQUEsTUFBTTtnQkFDSixPQUFPLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQzNCLENBQUM7Ozs7WUFDRCxVQUFBLEdBQUcsSUFBSSxPQUFBLE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEVBQWhCLENBQWdCLEVBQ3hCLENBQUM7UUFDSixDQUFDOzs7O1FBQ0QsVUFBQSxHQUFHLElBQUksT0FBQSxPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxFQUFoQixDQUFnQixFQUN4QixDQUFDO0lBQ0osQ0FBQzs7OztJQUVELDBDQUFlOzs7SUFBZjtRQUFBLGlCQU9DO1FBTkMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsU0FBUzs7OztRQUN4RSxVQUFBLElBQUk7WUFDRixLQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN4QyxDQUFDOzs7O1FBQ0QsVUFBQSxHQUFHLElBQUksT0FBQSxPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxFQUFoQixDQUFnQixFQUN4QixDQUFDO0lBQ0osQ0FBQzs7Z0JBcENGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsZUFBZTtvQkFDekIsd0pBQXdDOztpQkFFekM7Ozs7Z0JBUFEsYUFBYTs7OzhCQVNuQixLQUFLOzhCQUNMLEtBQUs7bUNBQ0wsTUFBTTs7SUE2QlQsdUJBQUM7Q0FBQSxBQXJDRCxJQXFDQztTQWhDWSxnQkFBZ0I7OztJQUMzQix1Q0FBNkI7O0lBQzdCLHVDQUE2Qjs7SUFDN0IsNENBQXFEOzs7OztJQUVyRCxpQ0FBc0I7Ozs7O0lBRVYseUNBQW9DIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tbW9uU2VydmljZSB9IGZyb20gJy4vLi4vY29tbW9uLnNlcnZpY2UnO1xuaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIElucHV0LCBPdXRwdXQsIEV2ZW50RW1pdHRlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdwY3JmLWNwLWlucHV0JyxcbiAgdGVtcGxhdGVVcmw6ICcuL2NwLWlucHV0LmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vY3AtaW5wdXQuY29tcG9uZW50LnNjc3MnXVxufSlcbmV4cG9ydCBjbGFzcyBDcElucHV0Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgQElucHV0KCkgcG9zdGFsX2NvZGU6IHN0cmluZztcbiAgQElucHV0KCkgcGxhY2Vob2xkZXI6IHN0cmluZztcbiAgQE91dHB1dCgpIHBvc3RhbF9jb2RlX2luZm8gPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcblxuICBwcml2YXRlIHRva2VuOiBzdHJpbmc7XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSBjb21tb25TZXJ2aWNlOiBDb21tb25TZXJ2aWNlKSB7fVxuXG4gIG5nT25Jbml0KCkge1xuICAgIHRoaXMuY29tbW9uU2VydmljZS5sb2dpbigpLnN1YnNjcmliZShcbiAgICAgIGxvZ2luRGF0YSA9PiB7XG4gICAgICAgIHRoaXMudG9rZW4gPSBsb2dpbkRhdGEudG9rZW47XG4gICAgICAgIHRoaXMuY29tbW9uU2VydmljZS5pbml0R2FpYSh0aGlzLnRva2VuKS5zdWJzY3JpYmUoXG4gICAgICAgICAgbG9hZGVkID0+IHtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKCdHYWlhIGluaXQnKTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIGVyciA9PiBjb25zb2xlLmxvZyhlcnIpXG4gICAgICAgICk7XG4gICAgICB9LFxuICAgICAgZXJyID0+IGNvbnNvbGUubG9nKGVycilcbiAgICApO1xuICB9XG5cbiAgY2hlY2tQb3N0YWxDb2RlKCk6IHZvaWQge1xuICAgIHRoaXMuY29tbW9uU2VydmljZS5jaGVja1Bvc3RhbENvZGUodGhpcy50b2tlbiwgdGhpcy5wb3N0YWxfY29kZSkuc3Vic2NyaWJlKFxuICAgICAgZGF0YSA9PiB7XG4gICAgICAgIHRoaXMucG9zdGFsX2NvZGVfaW5mby5lbWl0KGRhdGEuZGF0YSk7XG4gICAgICB9LFxuICAgICAgZXJyID0+IGNvbnNvbGUubG9nKGVycilcbiAgICApO1xuICB9XG59XG4iXX0=