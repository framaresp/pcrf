/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input } from '@angular/core';
var FooterComponent = /** @class */ (function () {
    function FooterComponent() {
    }
    /**
     * @return {?}
     */
    FooterComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () { };
    /**
     * @param {?} link
     * @param {?} event
     * @return {?}
     */
    FooterComponent.prototype.openIconLinks = /**
     * @param {?} link
     * @param {?} event
     * @return {?}
     */
    function (link, event) {
        link.click.call(event);
    };
    FooterComponent.decorators = [
        { type: Component, args: [{
                    selector: 'pcrf-footer',
                    template: "<div class=\"mfc-footer__main\">\r\n  <div class=\"mfc-footer__main-upper\">\r\n    <ul class=\"mfc-footer__main--list-links\">\r\n      <li\r\n        *ngFor=\"let link of left_links\"\r\n        class=\"mfc-footer__main--list-links__element\"\r\n      >\r\n        <a class=\"mfc-footer__link\" (click)=\"openIconLinks(link, $event)\">\r\n          {{ link.label }}\r\n        </a>\r\n        <div class=\"mfc-u-div-inline\">|</div>\r\n      </li>\r\n    </ul>\r\n  </div>\r\n  <ul class=\"mfc-footer__main--list-icon-links\">\r\n    <li *ngFor=\"let link of right_links\" class=\"mfc-footer__main__link\">\r\n      <a\r\n        [title]=\"link.label\"\r\n        class=\"mfc-footer__link\"\r\n        (click)=\"openIconLinks(link, $event)\"\r\n      >\r\n        <mat-icon\r\n          fontSet=\"fa\"\r\n          [fontIcon]=\"link.icon\"\r\n          class=\"mfc-icon\"\r\n        ></mat-icon>\r\n        {{ link.label }}\r\n      </a>\r\n    </li>\r\n  </ul>\r\n  <div class=\"mfc-footer__separator\"></div>\r\n  <div class=\"mfc-footer__copyright\">\r\n    <p ng-bind-html=\"copyright\" class=\"mfc-footer__copyright--no-text-link\">\r\n      {{ footer_text }}\r\n    </p>\r\n  </div>\r\n</div>\r\n",
                    styles: [":host{background-color:#d81e05;display:block;padding:0 12px;overflow:hidden}:host .mfc-footer__main{margin:0 auto;max-width:1200px;padding:20px 0}:host .mfc-footer__main .mfc-footer__main--list-icon-links{box-sizing:border-box;float:right;margin-top:-5px;text-align:right}:host .mfc-footer__main .mfc-footer__main--list-icon-links .mfc-footer__main__link{box-sizing:border-box;display:inline;margin:0 0 0 25px}:host .mfc-footer__main .mfc-footer__main--list-icon-links .mfc-footer__main__link .mfc-footer__link{background-color:rgba(0,0,0,0);color:#fff;font-family:noto_sansregular,sans-serif;-moz-text-size-adjust:100%;-ms-text-size-adjust:100%;cursor:pointer}:host .mfc-footer__main .mfc-footer__main-upper{float:left;overflow:hidden;margin:-5px 0 0}:host .mfc-footer__main .mfc-footer__main-upper .mfc-footer__main--list-links{margin:0;padding:0}:host .mfc-footer__main .mfc-footer__main-upper .mfc-footer__main--list-links .mfc-footer__main--list-links__element{list-style:none;float:left}:host .mfc-footer__main .mfc-footer__main-upper .mfc-footer__main--list-links .mfc-footer__main--list-links__element:last-of-type .mfc-u-div-inline{display:none}:host .mfc-footer__main .mfc-footer__main-upper .mfc-footer__main--list-links .mfc-footer__main--list-links__element .mfc-footer__link{color:#fff;font-family:noto_sansregular,sans-serif;font-size:14px;font-weight:600;text-transform:uppercase;cursor:pointer}:host .mfc-footer__main .mfc-footer__main-upper .mfc-footer__main--list-links .mfc-footer__main--list-links__element .mfc-u-div-inline{color:#fff;display:inline;padding:0 5px;font-weight:600}:host .mfc-footer__main .mfc-footer__separator{clear:both;height:1px;background:linear-gradient(to right,rgba(255,255,255,0) 0,#fff 10%,#fff 90%,rgba(255,255,255,0) 100%)}:host .mfc-footer__main .mfc-footer__copyright{clear:both;float:left;display:block;width:100%}:host .mfc-footer__main .mfc-footer__copyright .mfc-footer__copyright--no-text-link{display:flex;justify-content:center;color:#fff;font-size:13px}@media (max-width:867px){:host .mfc-footer__main .mfc-footer__main-upper{display:flex;justify-content:center;width:100%}:host .mfc-footer__main .mfc-footer__main--list-icon-links{display:flex;flex-wrap:wrap;width:100%;padding:10px 0 5px;margin:0}:host .mfc-footer__main .mfc-footer__main--list-icon-links .mfc-footer__main__link{flex:auto;margin:0;text-align:center}}"]
                }] }
    ];
    /** @nocollapse */
    FooterComponent.ctorParameters = function () { return []; };
    FooterComponent.propDecorators = {
        left_links: [{ type: Input }],
        right_links: [{ type: Input }],
        footer_text: [{ type: Input }]
    };
    return FooterComponent;
}());
export { FooterComponent };
if (false) {
    /** @type {?} */
    FooterComponent.prototype.left_links;
    /** @type {?} */
    FooterComponent.prototype.right_links;
    /** @type {?} */
    FooterComponent.prototype.footer_text;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9vdGVyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BwY3JmL2NvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9mb290ZXIvZm9vdGVyLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxLQUFLLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFHekQ7SUFXRTtJQUFlLENBQUM7Ozs7SUFFaEIsa0NBQVE7OztJQUFSLGNBQVksQ0FBQzs7Ozs7O0lBRWIsdUNBQWE7Ozs7O0lBQWIsVUFBYyxJQUFJLEVBQUUsS0FBSztRQUN2QixJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUN6QixDQUFDOztnQkFqQkYsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxhQUFhO29CQUN2Qixnc0NBQXNDOztpQkFFdkM7Ozs7OzZCQUVFLEtBQUs7OEJBQ0wsS0FBSzs4QkFFTCxLQUFLOztJQVNSLHNCQUFDO0NBQUEsQUFsQkQsSUFrQkM7U0FiWSxlQUFlOzs7SUFDMUIscUNBQWdDOztJQUNoQyxzQ0FBaUM7O0lBRWpDLHNDQUE2QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgSWNvbkxpbmsgfSBmcm9tICcuLi9tb2RlbHMvaWNvbi1saW5rJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAncGNyZi1mb290ZXInLFxuICB0ZW1wbGF0ZVVybDogJy4vZm9vdGVyLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vZm9vdGVyLmNvbXBvbmVudC5zY3NzJ11cbn0pXG5leHBvcnQgY2xhc3MgRm9vdGVyQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgQElucHV0KCkgbGVmdF9saW5rczogSWNvbkxpbmtbXTtcbiAgQElucHV0KCkgcmlnaHRfbGlua3M6IEljb25MaW5rW107XG5cbiAgQElucHV0KCkgZm9vdGVyX3RleHQ6IHN0cmluZztcblxuICBjb25zdHJ1Y3RvcigpIHt9XG5cbiAgbmdPbkluaXQoKSB7fVxuXG4gIG9wZW5JY29uTGlua3MobGluaywgZXZlbnQpIHtcbiAgICBsaW5rLmNsaWNrLmNhbGwoZXZlbnQpO1xuICB9XG59XG4iXX0=