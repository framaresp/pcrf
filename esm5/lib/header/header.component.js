/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input } from '@angular/core';
var HeaderComponent = /** @class */ (function () {
    function HeaderComponent() {
    }
    /**
     * @return {?}
     */
    HeaderComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () { };
    /**
     * @param {?} event
     * @return {?}
     */
    HeaderComponent.prototype.loco_clicked = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        this.loco_click.call(event);
    };
    /**
     * @param {?} link
     * @param {?} event
     * @return {?}
     */
    HeaderComponent.prototype.openIconLinks = /**
     * @param {?} link
     * @param {?} event
     * @return {?}
     */
    function (link, event) {
        link.click.call(event);
    };
    HeaderComponent.decorators = [
        { type: Component, args: [{
                    selector: 'pcrf-header',
                    template: "<div class=\"mfc-header__content\">\n  <div class=\"mfc-header__content__main\">\n    <a\n      [title]=\"logo_title\"\n      (click)=\"loco_clicked($event)\"\n      class=\"mfc-header__content__main__link\"\n    >\n      <img class=\"mc-header__content__main__logo\" [src]=\"logo_url\" />\n    </a>\n  </div>\n  <div class=\"mfc-header__content__heading\">\n    <h1 class=\"mfc-header__content__heading__element\">\n      {{ title }}\n    </h1>\n    <h2 *ngIf=\"subtitle\" class=\"mfc-header__content__heading__text\">\n      {{ subtitle }}\n    </h2>\n  </div>\n  <section class=\"mfc-header__content__contact\">\n    <div *ngIf=\"contact_phone\" class=\"mfc-header__content__contact__phone\">\n      <p class=\"mfc-header__content__contact__phone__text\">\n        <mat-icon\n          fontSet=\"fa\"\n          fontIcon=\"fa-phone\"\n          class=\"mfc-header__content__contact__phone__icon\"\n        ></mat-icon>\n        {{ contact_phone }}\n      </p>\n    </div>\n    <ul *ngIf=\"icon_links\" class=\"mfc-header__content__contact__list\">\n      <li\n        *ngFor=\"let link of icon_links\"\n        class=\"mfc-header__content__contact__list__element\"\n      >\n        <a\n          (click)=\"openIconLinks(link, $event)\"\n          class=\"mfc-header__content__link\"\n        >\n          <mat-icon\n            fontSet=\"fa\"\n            [fontIcon]=\"link.icon\"\n            class=\"mfc-icon\"\n          ></mat-icon>\n          <div class=\"mfc-u-inline mfc-header__content__contact__list__text\">\n            {{ link.label }}\n          </div>\n        </a>\n      </li>\n    </ul>\n  </section>\n</div>\n",
                    styles: [":host{background:#d81e05;display:block}:host .mfc-header__content{display:flex;align-items:center;flex-wrap:wrap;color:#fff;margin:0 auto;padding:24px 18px;max-width:1200px;overflow:hidden}:host .mfc-header__content .mfc-header__content__main{flex:25%;padding:0 15px 0 0;color:#fff;float:left}:host .mfc-header__content .mfc-header__content__main .mfc-header__content__main__link .mc-header__content__main__logo{cursor:pointer}:host .mfc-header__content .mfc-header__content__heading{display:flex;justify-content:center;flex-wrap:wrap;flex:25%;float:left;padding:0 15px 0 0;text-align:center}:host .mfc-header__content .mfc-header__content__heading .mfc-header__content__heading__element{flex:100%;font-size:25px;margin:0}:host .mfc-header__content .mfc-header__content__heading .mfc-header__content__heading__text{flex:100%;display:table-cell;font-size:18px;font-weight:500;padding:15px 0 0;margin:0;line-height:27px;vertical-align:middle}:host .mfc-header__content .mfc-header__content__contact{flex:auto;float:right;margin-top:-2px}:host .mfc-header__content .mfc-header__content__contact mat-icon{width:auto;height:auto}:host .mfc-header__content .mfc-header__content__contact .mfc-header__content__contact__phone .mfc-header__content__contact__phone__text{font-family:noto_sansregular,sans-serif;font-size:25px;font-weight:700;margin:0;text-align:right}:host .mfc-header__content .mfc-header__content__contact .mfc-header__content__contact__list{display:flex;justify-content:flex-end;margin:0;padding:15px 0 0;list-style:none}:host .mfc-header__content .mfc-header__content__contact .mfc-header__content__contact__list .mfc-header__content__contact__list__element{box-sizing:border-box;padding:0 8px;float:left}:host .mfc-header__content .mfc-header__content__contact .mfc-header__content__contact__list .mfc-header__content__contact__list__element:first-of-type{padding:0 8px 0 0}:host .mfc-header__content .mfc-header__content__contact .mfc-header__content__contact__list .mfc-header__content__contact__list__element:last-of-type{padding:0 0 0 8px}:host .mfc-header__content .mfc-header__content__contact .mfc-header__content__contact__list .mfc-header__content__contact__list__element .mfc-header__content__link{text-align:center}:host .mfc-header__content .mfc-header__content__contact .mfc-header__content__contact__list .mfc-header__content__contact__list__element .mfc-header__content__link .mfc-header__content__contact__list__text{display:inline;font-family:noto_sansregular,sans-serif;font-size:14px}@media (max-width:867px){:host .mfc-header__content{padding:5px}:host .mfc-header__content .mfc-header__content__main{display:flex;justify-content:center;flex:33%;padding:0}:host .mfc-header__content .mfc-header__content__main .mfc-header__content__main__link{padding:0 15px 0 0}:host .mfc-header__content .mfc-header__content__main .mfc-header__content__main__link .mc-header__content__main__logo{width:100%}:host .mfc-header__content .mfc-header__content__heading{flex:33%;padding:0}:host .mfc-header__content .mfc-header__content__heading .mfc-header__content__heading__element{font-size:14px}:host .mfc-header__content .mfc-header__content__heading .mfc-header__content__heading__text{padding:0;margin:0;font-size:12px}:host .mfc-header__content .mfc-header__content__contact{flex:33%;display:flex;justify-content:center;flex-wrap:wrap}:host .mfc-header__content .mfc-header__content__contact .mfc-header__content__contact__phone{flex:100%}:host .mfc-header__content .mfc-header__content__contact .mfc-header__content__contact__phone .mfc-header__content__contact__phone__text{font-size:13px;text-align:center}:host .mfc-header__content .mfc-header__content__contact .mfc-header__content__contact__list{display:none}}"]
                }] }
    ];
    /** @nocollapse */
    HeaderComponent.ctorParameters = function () { return []; };
    HeaderComponent.propDecorators = {
        logo_url: [{ type: Input }],
        logo_title: [{ type: Input }],
        loco_click: [{ type: Input }],
        title: [{ type: Input }],
        subtitle: [{ type: Input }],
        contact_phone: [{ type: Input }],
        icon_links: [{ type: Input }]
    };
    return HeaderComponent;
}());
export { HeaderComponent };
if (false) {
    /** @type {?} */
    HeaderComponent.prototype.logo_url;
    /** @type {?} */
    HeaderComponent.prototype.logo_title;
    /** @type {?} */
    HeaderComponent.prototype.loco_click;
    /** @type {?} */
    HeaderComponent.prototype.title;
    /** @type {?} */
    HeaderComponent.prototype.subtitle;
    /** @type {?} */
    HeaderComponent.prototype.contact_phone;
    /** @type {?} */
    HeaderComponent.prototype.icon_links;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaGVhZGVyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BwY3JmL2NvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9oZWFkZXIvaGVhZGVyLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQ0EsT0FBTyxFQUFFLFNBQVMsRUFBVSxLQUFLLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFekQ7SUFpQkU7SUFBZSxDQUFDOzs7O0lBRWhCLGtDQUFROzs7SUFBUixjQUFZLENBQUM7Ozs7O0lBRWIsc0NBQVk7Ozs7SUFBWixVQUFhLEtBQUs7UUFDaEIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDOUIsQ0FBQzs7Ozs7O0lBRUQsdUNBQWE7Ozs7O0lBQWIsVUFBYyxJQUFJLEVBQUUsS0FBSztRQUN2QixJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUN6QixDQUFDOztnQkEzQkYsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxhQUFhO29CQUN2QiwybURBQXNDOztpQkFFdkM7Ozs7OzJCQUVFLEtBQUs7NkJBQ0wsS0FBSzs2QkFDTCxLQUFLO3dCQUVMLEtBQUs7MkJBQ0wsS0FBSztnQ0FFTCxLQUFLOzZCQUVMLEtBQUs7O0lBYVIsc0JBQUM7Q0FBQSxBQTVCRCxJQTRCQztTQXZCWSxlQUFlOzs7SUFDMUIsbUNBQTBCOztJQUMxQixxQ0FBNEI7O0lBQzVCLHFDQUE4Qjs7SUFFOUIsZ0NBQXVCOztJQUN2QixtQ0FBMEI7O0lBRTFCLHdDQUErQjs7SUFFL0IscUNBQWdDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSWNvbkxpbmsgfSBmcm9tICcuLy4uL21vZGVscy9pY29uLWxpbmsnO1xuaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ3BjcmYtaGVhZGVyJyxcbiAgdGVtcGxhdGVVcmw6ICcuL2hlYWRlci5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL2hlYWRlci5jb21wb25lbnQuc2NzcyddXG59KVxuZXhwb3J0IGNsYXNzIEhlYWRlckNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gIEBJbnB1dCgpIGxvZ29fdXJsOiBzdHJpbmc7XG4gIEBJbnB1dCgpIGxvZ29fdGl0bGU6IHN0cmluZztcbiAgQElucHV0KCkgbG9jb19jbGljazogRnVuY3Rpb247XG5cbiAgQElucHV0KCkgdGl0bGU6IHN0cmluZztcbiAgQElucHV0KCkgc3VidGl0bGU6IHN0cmluZztcblxuICBASW5wdXQoKSBjb250YWN0X3Bob25lOiBzdHJpbmc7XG5cbiAgQElucHV0KCkgaWNvbl9saW5rczogSWNvbkxpbmtbXTtcblxuICBjb25zdHJ1Y3RvcigpIHt9XG5cbiAgbmdPbkluaXQoKSB7fVxuXG4gIGxvY29fY2xpY2tlZChldmVudCkge1xuICAgIHRoaXMubG9jb19jbGljay5jYWxsKGV2ZW50KTtcbiAgfVxuXG4gIG9wZW5JY29uTGlua3MobGluaywgZXZlbnQpIHtcbiAgICBsaW5rLmNsaWNrLmNhbGwoZXZlbnQpO1xuICB9XG59XG4iXX0=