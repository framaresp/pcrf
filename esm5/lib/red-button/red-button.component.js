/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input, EventEmitter, Output } from '@angular/core';
var RedButtonComponent = /** @class */ (function () {
    function RedButtonComponent() {
        this.event = new EventEmitter();
    }
    /**
     * @return {?}
     */
    RedButtonComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () { };
    /**
     * @param {?} event
     * @return {?}
     */
    RedButtonComponent.prototype.action = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        this.event.emit(event);
    };
    RedButtonComponent.decorators = [
        { type: Component, args: [{
                    selector: 'pcrf-red-button',
                    template: "<button\n  mat-raised-button\n  color=\"warn\"\n  [disabled]=\"disabled\"\n  (click)=\"action($event)\"\n>\n  {{ text }}\n</button>\n",
                    styles: [""]
                }] }
    ];
    /** @nocollapse */
    RedButtonComponent.ctorParameters = function () { return []; };
    RedButtonComponent.propDecorators = {
        disabled: [{ type: Input }],
        event: [{ type: Output }],
        text: [{ type: Input }]
    };
    return RedButtonComponent;
}());
export { RedButtonComponent };
if (false) {
    /** @type {?} */
    RedButtonComponent.prototype.disabled;
    /** @type {?} */
    RedButtonComponent.prototype.event;
    /** @type {?} */
    RedButtonComponent.prototype.text;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVkLWJ1dHRvbi5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AcGNyZi9jb21tb24vIiwic291cmNlcyI6WyJsaWIvcmVkLWJ1dHRvbi9yZWQtYnV0dG9uLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxLQUFLLEVBQUUsWUFBWSxFQUFFLE1BQU0sRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUUvRTtJQVVFO1FBSFUsVUFBSyxHQUFHLElBQUksWUFBWSxFQUFXLENBQUM7SUFHL0IsQ0FBQzs7OztJQUVoQixxQ0FBUTs7O0lBQVIsY0FBWSxDQUFDOzs7OztJQUViLG1DQUFNOzs7O0lBQU4sVUFBTyxLQUFLO1FBQ1YsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDekIsQ0FBQzs7Z0JBaEJGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsaUJBQWlCO29CQUMzQixpSkFBMEM7O2lCQUUzQzs7Ozs7MkJBRUUsS0FBSzt3QkFDTCxNQUFNO3VCQUNOLEtBQUs7O0lBU1IseUJBQUM7Q0FBQSxBQWpCRCxJQWlCQztTQVpZLGtCQUFrQjs7O0lBQzdCLHNDQUEyQjs7SUFDM0IsbUNBQThDOztJQUM5QyxrQ0FBc0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQsIEV2ZW50RW1pdHRlciwgT3V0cHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ3BjcmYtcmVkLWJ1dHRvbicsXG4gIHRlbXBsYXRlVXJsOiAnLi9yZWQtYnV0dG9uLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vcmVkLWJ1dHRvbi5jb21wb25lbnQuc2NzcyddXG59KVxuZXhwb3J0IGNsYXNzIFJlZEJ1dHRvbkNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gIEBJbnB1dCgpIGRpc2FibGVkOiBCb29sZWFuO1xuICBAT3V0cHV0KCkgZXZlbnQgPSBuZXcgRXZlbnRFbWl0dGVyPGJvb2xlYW4+KCk7XG4gIEBJbnB1dCgpIHRleHQ6IHN0cmluZztcblxuICBjb25zdHJ1Y3RvcigpIHt9XG5cbiAgbmdPbkluaXQoKSB7fVxuXG4gIGFjdGlvbihldmVudCkge1xuICAgIHRoaXMuZXZlbnQuZW1pdChldmVudCk7XG4gIH1cbn1cbiJdfQ==