/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input } from '@angular/core';
var TextBoxComponent = /** @class */ (function () {
    function TextBoxComponent() {
    }
    /**
     * @return {?}
     */
    TextBoxComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () { };
    TextBoxComponent.decorators = [
        { type: Component, args: [{
                    selector: 'pcrf-text-box',
                    template: "<mat-card class=\"example-card\">\n  <mat-card-content>\n    <p>{{ text }}</p>\n  </mat-card-content>\n</mat-card>\n",
                    styles: [""]
                }] }
    ];
    /** @nocollapse */
    TextBoxComponent.ctorParameters = function () { return []; };
    TextBoxComponent.propDecorators = {
        text: [{ type: Input }]
    };
    return TextBoxComponent;
}());
export { TextBoxComponent };
if (false) {
    /** @type {?} */
    TextBoxComponent.prototype.text;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGV4dC1ib3guY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQHBjcmYvY29tbW9uLyIsInNvdXJjZXMiOlsibGliL3RleHQtYm94L3RleHQtYm94LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxLQUFLLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFekQ7SUFRRTtJQUFlLENBQUM7Ozs7SUFFaEIsbUNBQVE7OztJQUFSLGNBQVksQ0FBQzs7Z0JBVmQsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxlQUFlO29CQUN6QixnSUFBd0M7O2lCQUV6Qzs7Ozs7dUJBRUUsS0FBSzs7SUFLUix1QkFBQztDQUFBLEFBWEQsSUFXQztTQU5ZLGdCQUFnQjs7O0lBQzNCLGdDQUFzQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdwY3JmLXRleHQtYm94JyxcbiAgdGVtcGxhdGVVcmw6ICcuL3RleHQtYm94LmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vdGV4dC1ib3guY29tcG9uZW50LnNjc3MnXVxufSlcbmV4cG9ydCBjbGFzcyBUZXh0Qm94Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgQElucHV0KCkgdGV4dDogc3RyaW5nO1xuXG4gIGNvbnN0cnVjdG9yKCkge31cblxuICBuZ09uSW5pdCgpIHt9XG59XG4iXX0=