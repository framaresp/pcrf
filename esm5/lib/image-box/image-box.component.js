/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input } from '@angular/core';
var ImageBoxComponent = /** @class */ (function () {
    function ImageBoxComponent() {
    }
    /**
     * @return {?}
     */
    ImageBoxComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () { };
    ImageBoxComponent.decorators = [
        { type: Component, args: [{
                    selector: 'pcrf-image-box',
                    template: "<mat-card class=\"example-card\">\n  <mat-card-header>\n    <mat-card-title>{{ title }}</mat-card-title>\n    <mat-card-subtitle>{{ subtitle }}</mat-card-subtitle>\n  </mat-card-header>\n  <img mat-card-image [src]=\"image_url\" alt=\"photo\" />\n</mat-card>\n",
                    styles: [""]
                }] }
    ];
    /** @nocollapse */
    ImageBoxComponent.ctorParameters = function () { return []; };
    ImageBoxComponent.propDecorators = {
        title: [{ type: Input }],
        subtitle: [{ type: Input }],
        image_url: [{ type: Input }]
    };
    return ImageBoxComponent;
}());
export { ImageBoxComponent };
if (false) {
    /** @type {?} */
    ImageBoxComponent.prototype.title;
    /** @type {?} */
    ImageBoxComponent.prototype.subtitle;
    /** @type {?} */
    ImageBoxComponent.prototype.image_url;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW1hZ2UtYm94LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BwY3JmL2NvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9pbWFnZS1ib3gvaW1hZ2UtYm94LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxLQUFLLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFekQ7SUFVRTtJQUFlLENBQUM7Ozs7SUFFaEIsb0NBQVE7OztJQUFSLGNBQVksQ0FBQzs7Z0JBWmQsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxnQkFBZ0I7b0JBQzFCLGdSQUF5Qzs7aUJBRTFDOzs7Ozt3QkFFRSxLQUFLOzJCQUNMLEtBQUs7NEJBQ0wsS0FBSzs7SUFLUix3QkFBQztDQUFBLEFBYkQsSUFhQztTQVJZLGlCQUFpQjs7O0lBQzVCLGtDQUF1Qjs7SUFDdkIscUNBQTBCOztJQUMxQixzQ0FBMkIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAncGNyZi1pbWFnZS1ib3gnLFxuICB0ZW1wbGF0ZVVybDogJy4vaW1hZ2UtYm94LmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vaW1hZ2UtYm94LmNvbXBvbmVudC5zY3NzJ11cbn0pXG5leHBvcnQgY2xhc3MgSW1hZ2VCb3hDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICBASW5wdXQoKSB0aXRsZTogc3RyaW5nO1xuICBASW5wdXQoKSBzdWJ0aXRsZTogc3RyaW5nO1xuICBASW5wdXQoKSBpbWFnZV91cmw6IHN0cmluZztcblxuICBjb25zdHJ1Y3RvcigpIHt9XG5cbiAgbmdPbkluaXQoKSB7fVxufVxuIl19