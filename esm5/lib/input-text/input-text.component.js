/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input, Output, EventEmitter } from '@angular/core';
var InputTextComponent = /** @class */ (function () {
    function InputTextComponent() {
        this.valueChange = new EventEmitter();
    }
    /**
     * @return {?}
     */
    InputTextComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () { };
    /**
     * @param {?} event
     * @return {?}
     */
    InputTextComponent.prototype.onDataChange = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        this.value = event.target.value;
        this.valueChange.emit(this.value);
    };
    InputTextComponent.decorators = [
        { type: Component, args: [{
                    selector: 'pcrf-input-text',
                    template: "<mat-form-field class=\"example-full-width\">\n  <input\n    matInput\n    [placeholder]=\"placeholder\"\n    [value]=\"value\"\n    (change)=\"onDataChange($event)\"\n  />\n</mat-form-field>\n",
                    styles: [""]
                }] }
    ];
    /** @nocollapse */
    InputTextComponent.ctorParameters = function () { return []; };
    InputTextComponent.propDecorators = {
        placeholder: [{ type: Input }],
        value: [{ type: Input }],
        valueChange: [{ type: Output }]
    };
    return InputTextComponent;
}());
export { InputTextComponent };
if (false) {
    /** @type {?} */
    InputTextComponent.prototype.placeholder;
    /** @type {?} */
    InputTextComponent.prototype.value;
    /** @type {?} */
    InputTextComponent.prototype.valueChange;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5wdXQtdGV4dC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AcGNyZi9jb21tb24vIiwic291cmNlcyI6WyJsaWIvaW5wdXQtdGV4dC9pbnB1dC10ZXh0LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxLQUFLLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUUvRTtJQVVFO1FBRlUsZ0JBQVcsR0FBRyxJQUFJLFlBQVksRUFBVSxDQUFDO0lBRXBDLENBQUM7Ozs7SUFFaEIscUNBQVE7OztJQUFSLGNBQVksQ0FBQzs7Ozs7SUFFYix5Q0FBWTs7OztJQUFaLFVBQWEsS0FBSztRQUNoQixJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDO1FBQ2hDLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNwQyxDQUFDOztnQkFqQkYsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxpQkFBaUI7b0JBQzNCLDZNQUEwQzs7aUJBRTNDOzs7Ozs4QkFFRSxLQUFLO3dCQUNMLEtBQUs7OEJBQ0wsTUFBTTs7SUFVVCx5QkFBQztDQUFBLEFBbEJELElBa0JDO1NBYlksa0JBQWtCOzs7SUFDN0IseUNBQTZCOztJQUM3QixtQ0FBdUI7O0lBQ3ZCLHlDQUFtRCIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbnB1dCwgT3V0cHV0LCBFdmVudEVtaXR0ZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAncGNyZi1pbnB1dC10ZXh0JyxcbiAgdGVtcGxhdGVVcmw6ICcuL2lucHV0LXRleHQuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9pbnB1dC10ZXh0LmNvbXBvbmVudC5zY3NzJ11cbn0pXG5leHBvcnQgY2xhc3MgSW5wdXRUZXh0Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgQElucHV0KCkgcGxhY2Vob2xkZXI6IHN0cmluZztcbiAgQElucHV0KCkgdmFsdWU6IHN0cmluZztcbiAgQE91dHB1dCgpIHZhbHVlQ2hhbmdlID0gbmV3IEV2ZW50RW1pdHRlcjxTdHJpbmc+KCk7XG5cbiAgY29uc3RydWN0b3IoKSB7fVxuXG4gIG5nT25Jbml0KCkge31cblxuICBvbkRhdGFDaGFuZ2UoZXZlbnQpIHtcbiAgICB0aGlzLnZhbHVlID0gZXZlbnQudGFyZ2V0LnZhbHVlO1xuICAgIHRoaXMudmFsdWVDaGFuZ2UuZW1pdCh0aGlzLnZhbHVlKTtcbiAgfVxufVxuIl19