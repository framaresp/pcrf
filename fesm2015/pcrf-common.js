import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { Component, Input, EventEmitter, Output, Injectable, NgModule, defineInjectable, inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class RedButtonComponent {
    constructor() {
        this.event = new EventEmitter();
    }
    /**
     * @return {?}
     */
    ngOnInit() { }
    /**
     * @param {?} event
     * @return {?}
     */
    action(event) {
        this.event.emit(event);
    }
}
RedButtonComponent.decorators = [
    { type: Component, args: [{
                selector: 'pcrf-red-button',
                template: "<button\n  mat-raised-button\n  color=\"warn\"\n  [disabled]=\"disabled\"\n  (click)=\"action($event)\"\n>\n  {{ text }}\n</button>\n",
                styles: [""]
            }] }
];
/** @nocollapse */
RedButtonComponent.ctorParameters = () => [];
RedButtonComponent.propDecorators = {
    disabled: [{ type: Input }],
    event: [{ type: Output }],
    text: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class HeaderComponent {
    constructor() { }
    /**
     * @return {?}
     */
    ngOnInit() { }
    /**
     * @param {?} event
     * @return {?}
     */
    loco_clicked(event) {
        this.loco_click.call(event);
    }
    /**
     * @param {?} link
     * @param {?} event
     * @return {?}
     */
    openIconLinks(link, event) {
        link.click.call(event);
    }
}
HeaderComponent.decorators = [
    { type: Component, args: [{
                selector: 'pcrf-header',
                template: "<div class=\"mfc-header__content\">\n  <div class=\"mfc-header__content__main\">\n    <a\n      [title]=\"logo_title\"\n      (click)=\"loco_clicked($event)\"\n      class=\"mfc-header__content__main__link\"\n    >\n      <img class=\"mc-header__content__main__logo\" [src]=\"logo_url\" />\n    </a>\n  </div>\n  <div class=\"mfc-header__content__heading\">\n    <h1 class=\"mfc-header__content__heading__element\">\n      {{ title }}\n    </h1>\n    <h2 *ngIf=\"subtitle\" class=\"mfc-header__content__heading__text\">\n      {{ subtitle }}\n    </h2>\n  </div>\n  <section class=\"mfc-header__content__contact\">\n    <div *ngIf=\"contact_phone\" class=\"mfc-header__content__contact__phone\">\n      <p class=\"mfc-header__content__contact__phone__text\">\n        <mat-icon\n          fontSet=\"fa\"\n          fontIcon=\"fa-phone\"\n          class=\"mfc-header__content__contact__phone__icon\"\n        ></mat-icon>\n        {{ contact_phone }}\n      </p>\n    </div>\n    <ul *ngIf=\"icon_links\" class=\"mfc-header__content__contact__list\">\n      <li\n        *ngFor=\"let link of icon_links\"\n        class=\"mfc-header__content__contact__list__element\"\n      >\n        <a\n          (click)=\"openIconLinks(link, $event)\"\n          class=\"mfc-header__content__link\"\n        >\n          <mat-icon\n            fontSet=\"fa\"\n            [fontIcon]=\"link.icon\"\n            class=\"mfc-icon\"\n          ></mat-icon>\n          <div class=\"mfc-u-inline mfc-header__content__contact__list__text\">\n            {{ link.label }}\n          </div>\n        </a>\n      </li>\n    </ul>\n  </section>\n</div>\n",
                styles: [":host{background:#d81e05;display:block}:host .mfc-header__content{display:flex;align-items:center;flex-wrap:wrap;color:#fff;margin:0 auto;padding:24px 18px;max-width:1200px;overflow:hidden}:host .mfc-header__content .mfc-header__content__main{flex:25%;padding:0 15px 0 0;color:#fff;float:left}:host .mfc-header__content .mfc-header__content__main .mfc-header__content__main__link .mc-header__content__main__logo{cursor:pointer}:host .mfc-header__content .mfc-header__content__heading{display:flex;justify-content:center;flex-wrap:wrap;flex:25%;float:left;padding:0 15px 0 0;text-align:center}:host .mfc-header__content .mfc-header__content__heading .mfc-header__content__heading__element{flex:100%;font-size:25px;margin:0}:host .mfc-header__content .mfc-header__content__heading .mfc-header__content__heading__text{flex:100%;display:table-cell;font-size:18px;font-weight:500;padding:15px 0 0;margin:0;line-height:27px;vertical-align:middle}:host .mfc-header__content .mfc-header__content__contact{flex:auto;float:right;margin-top:-2px}:host .mfc-header__content .mfc-header__content__contact mat-icon{width:auto;height:auto}:host .mfc-header__content .mfc-header__content__contact .mfc-header__content__contact__phone .mfc-header__content__contact__phone__text{font-family:noto_sansregular,sans-serif;font-size:25px;font-weight:700;margin:0;text-align:right}:host .mfc-header__content .mfc-header__content__contact .mfc-header__content__contact__list{display:flex;justify-content:flex-end;margin:0;padding:15px 0 0;list-style:none}:host .mfc-header__content .mfc-header__content__contact .mfc-header__content__contact__list .mfc-header__content__contact__list__element{box-sizing:border-box;padding:0 8px;float:left}:host .mfc-header__content .mfc-header__content__contact .mfc-header__content__contact__list .mfc-header__content__contact__list__element:first-of-type{padding:0 8px 0 0}:host .mfc-header__content .mfc-header__content__contact .mfc-header__content__contact__list .mfc-header__content__contact__list__element:last-of-type{padding:0 0 0 8px}:host .mfc-header__content .mfc-header__content__contact .mfc-header__content__contact__list .mfc-header__content__contact__list__element .mfc-header__content__link{text-align:center}:host .mfc-header__content .mfc-header__content__contact .mfc-header__content__contact__list .mfc-header__content__contact__list__element .mfc-header__content__link .mfc-header__content__contact__list__text{display:inline;font-family:noto_sansregular,sans-serif;font-size:14px}@media (max-width:867px){:host .mfc-header__content{padding:5px}:host .mfc-header__content .mfc-header__content__main{display:flex;justify-content:center;flex:33%;padding:0}:host .mfc-header__content .mfc-header__content__main .mfc-header__content__main__link{padding:0 15px 0 0}:host .mfc-header__content .mfc-header__content__main .mfc-header__content__main__link .mc-header__content__main__logo{width:100%}:host .mfc-header__content .mfc-header__content__heading{flex:33%;padding:0}:host .mfc-header__content .mfc-header__content__heading .mfc-header__content__heading__element{font-size:14px}:host .mfc-header__content .mfc-header__content__heading .mfc-header__content__heading__text{padding:0;margin:0;font-size:12px}:host .mfc-header__content .mfc-header__content__contact{flex:33%;display:flex;justify-content:center;flex-wrap:wrap}:host .mfc-header__content .mfc-header__content__contact .mfc-header__content__contact__phone{flex:100%}:host .mfc-header__content .mfc-header__content__contact .mfc-header__content__contact__phone .mfc-header__content__contact__phone__text{font-size:13px;text-align:center}:host .mfc-header__content .mfc-header__content__contact .mfc-header__content__contact__list{display:none}}"]
            }] }
];
/** @nocollapse */
HeaderComponent.ctorParameters = () => [];
HeaderComponent.propDecorators = {
    logo_url: [{ type: Input }],
    logo_title: [{ type: Input }],
    loco_click: [{ type: Input }],
    title: [{ type: Input }],
    subtitle: [{ type: Input }],
    contact_phone: [{ type: Input }],
    icon_links: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class FooterComponent {
    constructor() { }
    /**
     * @return {?}
     */
    ngOnInit() { }
    /**
     * @param {?} link
     * @param {?} event
     * @return {?}
     */
    openIconLinks(link, event) {
        link.click.call(event);
    }
}
FooterComponent.decorators = [
    { type: Component, args: [{
                selector: 'pcrf-footer',
                template: "<div class=\"mfc-footer__main\">\r\n  <div class=\"mfc-footer__main-upper\">\r\n    <ul class=\"mfc-footer__main--list-links\">\r\n      <li\r\n        *ngFor=\"let link of left_links\"\r\n        class=\"mfc-footer__main--list-links__element\"\r\n      >\r\n        <a class=\"mfc-footer__link\" (click)=\"openIconLinks(link, $event)\">\r\n          {{ link.label }}\r\n        </a>\r\n        <div class=\"mfc-u-div-inline\">|</div>\r\n      </li>\r\n    </ul>\r\n  </div>\r\n  <ul class=\"mfc-footer__main--list-icon-links\">\r\n    <li *ngFor=\"let link of right_links\" class=\"mfc-footer__main__link\">\r\n      <a\r\n        [title]=\"link.label\"\r\n        class=\"mfc-footer__link\"\r\n        (click)=\"openIconLinks(link, $event)\"\r\n      >\r\n        <mat-icon\r\n          fontSet=\"fa\"\r\n          [fontIcon]=\"link.icon\"\r\n          class=\"mfc-icon\"\r\n        ></mat-icon>\r\n        {{ link.label }}\r\n      </a>\r\n    </li>\r\n  </ul>\r\n  <div class=\"mfc-footer__separator\"></div>\r\n  <div class=\"mfc-footer__copyright\">\r\n    <p ng-bind-html=\"copyright\" class=\"mfc-footer__copyright--no-text-link\">\r\n      {{ footer_text }}\r\n    </p>\r\n  </div>\r\n</div>\r\n",
                styles: [":host{background-color:#d81e05;display:block;padding:0 12px;overflow:hidden}:host .mfc-footer__main{margin:0 auto;max-width:1200px;padding:20px 0}:host .mfc-footer__main .mfc-footer__main--list-icon-links{box-sizing:border-box;float:right;margin-top:-5px;text-align:right}:host .mfc-footer__main .mfc-footer__main--list-icon-links .mfc-footer__main__link{box-sizing:border-box;display:inline;margin:0 0 0 25px}:host .mfc-footer__main .mfc-footer__main--list-icon-links .mfc-footer__main__link .mfc-footer__link{background-color:rgba(0,0,0,0);color:#fff;font-family:noto_sansregular,sans-serif;-moz-text-size-adjust:100%;-ms-text-size-adjust:100%;cursor:pointer}:host .mfc-footer__main .mfc-footer__main-upper{float:left;overflow:hidden;margin:-5px 0 0}:host .mfc-footer__main .mfc-footer__main-upper .mfc-footer__main--list-links{margin:0;padding:0}:host .mfc-footer__main .mfc-footer__main-upper .mfc-footer__main--list-links .mfc-footer__main--list-links__element{list-style:none;float:left}:host .mfc-footer__main .mfc-footer__main-upper .mfc-footer__main--list-links .mfc-footer__main--list-links__element:last-of-type .mfc-u-div-inline{display:none}:host .mfc-footer__main .mfc-footer__main-upper .mfc-footer__main--list-links .mfc-footer__main--list-links__element .mfc-footer__link{color:#fff;font-family:noto_sansregular,sans-serif;font-size:14px;font-weight:600;text-transform:uppercase;cursor:pointer}:host .mfc-footer__main .mfc-footer__main-upper .mfc-footer__main--list-links .mfc-footer__main--list-links__element .mfc-u-div-inline{color:#fff;display:inline;padding:0 5px;font-weight:600}:host .mfc-footer__main .mfc-footer__separator{clear:both;height:1px;background:linear-gradient(to right,rgba(255,255,255,0) 0,#fff 10%,#fff 90%,rgba(255,255,255,0) 100%)}:host .mfc-footer__main .mfc-footer__copyright{clear:both;float:left;display:block;width:100%}:host .mfc-footer__main .mfc-footer__copyright .mfc-footer__copyright--no-text-link{display:flex;justify-content:center;color:#fff;font-size:13px}@media (max-width:867px){:host .mfc-footer__main .mfc-footer__main-upper{display:flex;justify-content:center;width:100%}:host .mfc-footer__main .mfc-footer__main--list-icon-links{display:flex;flex-wrap:wrap;width:100%;padding:10px 0 5px;margin:0}:host .mfc-footer__main .mfc-footer__main--list-icon-links .mfc-footer__main__link{flex:auto;margin:0;text-align:center}}"]
            }] }
];
/** @nocollapse */
FooterComponent.ctorParameters = () => [];
FooterComponent.propDecorators = {
    left_links: [{ type: Input }],
    right_links: [{ type: Input }],
    footer_text: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class InputTextComponent {
    constructor() {
        this.valueChange = new EventEmitter();
    }
    /**
     * @return {?}
     */
    ngOnInit() { }
    /**
     * @param {?} event
     * @return {?}
     */
    onDataChange(event) {
        this.value = event.target.value;
        this.valueChange.emit(this.value);
    }
}
InputTextComponent.decorators = [
    { type: Component, args: [{
                selector: 'pcrf-input-text',
                template: "<mat-form-field class=\"example-full-width\">\n  <input\n    matInput\n    [placeholder]=\"placeholder\"\n    [value]=\"value\"\n    (change)=\"onDataChange($event)\"\n  />\n</mat-form-field>\n",
                styles: [""]
            }] }
];
/** @nocollapse */
InputTextComponent.ctorParameters = () => [];
InputTextComponent.propDecorators = {
    placeholder: [{ type: Input }],
    value: [{ type: Input }],
    valueChange: [{ type: Output }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class ImageBoxComponent {
    constructor() { }
    /**
     * @return {?}
     */
    ngOnInit() { }
}
ImageBoxComponent.decorators = [
    { type: Component, args: [{
                selector: 'pcrf-image-box',
                template: "<mat-card class=\"example-card\">\n  <mat-card-header>\n    <mat-card-title>{{ title }}</mat-card-title>\n    <mat-card-subtitle>{{ subtitle }}</mat-card-subtitle>\n  </mat-card-header>\n  <img mat-card-image [src]=\"image_url\" alt=\"photo\" />\n</mat-card>\n",
                styles: [""]
            }] }
];
/** @nocollapse */
ImageBoxComponent.ctorParameters = () => [];
ImageBoxComponent.propDecorators = {
    title: [{ type: Input }],
    subtitle: [{ type: Input }],
    image_url: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class TextBoxComponent {
    constructor() { }
    /**
     * @return {?}
     */
    ngOnInit() { }
}
TextBoxComponent.decorators = [
    { type: Component, args: [{
                selector: 'pcrf-text-box',
                template: "<mat-card class=\"example-card\">\n  <mat-card-content>\n    <p>{{ text }}</p>\n  </mat-card-content>\n</mat-card>\n",
                styles: [""]
            }] }
];
/** @nocollapse */
TextBoxComponent.ctorParameters = () => [];
TextBoxComponent.propDecorators = {
    text: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class CommonService {
    /**
     * @param {?} httpClient
     */
    constructor(httpClient) {
        this.httpClient = httpClient;
        this.httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            })
        };
    }
    /**
     * @param {?} token
     * @param {?} postalCode
     * @return {?}
     */
    checkPostalCode(token, postalCode) {
        return this.httpClient.get(`https://apisb.mapfre.com/srv/pocTarificadorVidaComeRound/components/mfcDeyde/codigoPostal`, {
            headers: new HttpHeaders({
                Authorization: `Bearer ${token}`
            }),
            params: new HttpParams().set('parameters', `{"cp":"${postalCode}","validatePostalCode":false}`),
            withCredentials: true
        });
    }
    /**
     * @return {?}
     */
    login() {
        return this.httpClient.post('https://apisb.mapfre.com/srv/pocTarificadorVidaComeRound/login', {}, {
            headers: new HttpHeaders({
                Authorization: 'Basic QVJDVVNBSTpNYXBmcmUyMDE4'
            })
        });
    }
    /**
     * @param {?} token
     * @return {?}
     */
    initGaia(token) {
        return this.httpClient.post('https://apisb.mapfre.com/srv/pocTarificadorVidaComeRound/register/globaldata', {
            data: {
                config: {
                    real: true,
                    product: 'Vida',
                    branch: 'Vida',
                    entity: 'Vida',
                    group: 'Vida',
                    geo: 'espaÃ±a',
                    core: 'Vida',
                    language: 'es-es',
                    currency: 'â¬',
                    dateFormat: '',
                    direction: false,
                    theme: 'Classic Left',
                    rv: '',
                    helpIcon: 'mfc-icon--info',
                    enterprise: 'Vida',
                    version: 'v4.12.3',
                    analytics: {
                        viewState: 'stepView',
                        viewShadowBox: 'popupView',
                        sendForm: 'formLink'
                    },
                    itemsPromotion: '1',
                    aggregators: ['confu'],
                    dataStackQuote: `lOJ14enR3T5G5/0EUXy2ke14T+vk99QWV7EBlPyt8RtWqLhJzcbouFNLMxxyjs81k89JHRGvMop2Y9SKjx1C5SLm0vzqc1cLboFEyse
              'auBWwbHXrtQgftGU6I5O5aslWGDQaPKVzpQqCEmXoKfEPlCvZyiULbHuF26gYLo4LMhjpPYnCTZlv+aRsjde6/aNFjKciQxd4pQg=`,
                    dataStackBuy: `8NRR4c21peCL37o6o8mBwMIrGj4FAYgFU3eceIKK97n7pIbxtqyMUO8AVC1j4e2/6jzZ2QK7vcUiNXIB/UTgl/hBfvHB+s53HZX7ua6M
              JPe90o3N6cLEaU3nv/HjLvEXADbTE9OpyfoidoD97Hfta0zeSB0k3yhDI6E12oIsMO6J5rRtXQd5Rq6mVF18CFYSQCb8sZIClrxZxys4j
              EI1OYTygUc+HUHWQpkN/5fGqIw5BImBH27ux9xXSfRXjuXzrQheTKx4qYQWhBaze6VqOnOrfJo6dmcPuU1Tsz7UpCzd1v5IiOa0T4YBbx
              YypXhDzgm44cFJseQeMNYL/tT+5W57Gh/hx0le4mTS6wpzZhZ/CVdrZTsZCBkl7HNrlBss9LFCYufTG6nOlUH8p17Re/8xmZu0+8/3DSM
              xoXEBBl2VFCRjpyfh8bdojaBSU/2LvUV8Xbc9e/q+9lyFmHq2soZTqMaWDuKa7gznO+JjBNzRS3/TUiWZUz8cgBbruTp5S1J89PbXhe0r
              iPUYZ3jYxez0dUW+I3V9WVsS1vqXrl8J8cXSS0EKgkTR5hUgejSwBMtWQPD7R7FOSqzt5UIV9+rKVnFI33Mm7e/O/ex09HwtuU/3KmNBL
              BsD9f/HCPtwiT+sSkA/Awfh6X2FNHC0KPVq9PNTTrgVkZ2iIY5DVa/khum8t0bGHYIxHJvXY+z+OUKKljwK5wnp0BMhzYRf47TGCA9w6q
              gv1m5+Ep2q5w2da0oqy6Gbxp7yqV3MXbnqKIiFfAquryA=`,
                    exitApp: {}
                },
                browser: {
                    platform: 'Windows',
                    navigator: {
                        navigatorType: 'Chrome',
                        navigatorVersion: '74.0.3729.108'
                    },
                    mobile: false
                }
            }
        }, {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                Authorization: `Bearer ${token}`
            }),
            withCredentials: true
        });
    }
    /**
     * @param {?} page_id
     * @return {?}
     */
    getTridionData(page_id) {
        return this.httpClient.get(`https://poc-api-mapfre.ciberexperis.net/api/PageContents/${page_id}`, this.httpOptions);
    }
    /**
     * @param {?} com_id
     * @return {?}
     */
    getTridionComponent(com_id) {
        com_id = com_id.replace('tcm:', '');
        return this.httpClient.get(`https://poc-api-mapfre.ciberexperis.net/api/Components/${com_id}`, this.httpOptions);
    }
    /**
     * @param {?} pub_id
     * @return {?}
     */
    getPublicationData(pub_id) {
        return this.httpClient.get(`https://poc-api-mapfre.ciberexperis.net/api/PublicationPageContents/${pub_id}`, this.httpOptions);
    }
    /**
     * @param {?} device
     * @return {?}
     */
    getSmartTarget(device) {
        return this.httpClient.post('https://poc-smarttarget-mapfre.ciberexperis.net/xo-promotions/_search?pretty', {
            query: {
                bool: {
                    must: [
                        { match: { scopePublication: 'tcm:0-3-1' } },
                        {
                            match: {
                                'triggers.triggerValues.stringValues': device
                            }
                        },
                        { match: { 'triggers.triggerValues.longValues': 114 } },
                        { match: { state: 'PENDING_ACTIVATION' } }
                    ]
                }
            }
        }, this.httpOptions);
    }
    // Create content in tridion
    /**
     * @param {?} publication_id
     * @param {?} page_title
     * @param {?} file_name
     * @param {?} pink_folder_id
     * @param {?} page_template_id
     * @return {?}
     */
    createPageInTridion(publication_id, page_title, file_name, pink_folder_id, page_template_id) {
        /** @type {?} */
        const body = {
            Id: 'tcm:0-0-0',
            Title: page_title,
            FileName: file_name,
            LocationInfo: {
                PublishLocationPath: '',
                PublishLocationUrl: '',
                PublishPath: '',
                ContextRepository: {
                    IdRef: publication_id,
                    Title: ''
                },
                OrganizationalItem: {
                    IdRef: pink_folder_id,
                    Title: ''
                },
                Path: '',
                WebDavUrl: ''
            },
            PageTemplate: {
                IdRef: page_template_id,
                Title: ''
            }
        };
        return this.httpClient.post('https://poc-api-mapfre.ciberexperis.net/api/Page', body, this.httpOptions);
    }
    /**
     * @param {?} yellow_folder_id
     * @param {?} schema_id
     * @param {?} title
     * @param {?} content
     * @return {?}
     */
    createComponentToPageInTridion(yellow_folder_id, schema_id, title, content) {
        /** @type {?} */
        const body = {
            Id: 'tcm:0-0-0',
            SchemaID: schema_id,
            Title: title,
            FolderID: yellow_folder_id,
            Content: content
        };
        return this.httpClient.post('https://poc-api-mapfre.ciberexperis.net/api/ComponentContents', body, this.httpOptions);
    }
    /**
     * @param {?} page_id
     * @param {?} components
     * @return {?}
     */
    addComponentToPageInTridion(page_id, components) {
        /** @type {?} */
        const body = {
            PageId: page_id,
            ComponentPresentations: components
        };
        return this.httpClient.put('https://poc-api-mapfre.ciberexperis.net/api/Page', body, this.httpOptions);
    }
}
CommonService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
CommonService.ctorParameters = () => [
    { type: HttpClient }
];
/** @nocollapse */ CommonService.ngInjectableDef = defineInjectable({ factory: function CommonService_Factory() { return new CommonService(inject(HttpClient)); }, token: CommonService, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class CpInputComponent {
    /**
     * @param {?} commonService
     */
    constructor(commonService) {
        this.commonService = commonService;
        this.postal_code_info = new EventEmitter();
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.commonService.login().subscribe((/**
         * @param {?} loginData
         * @return {?}
         */
        loginData => {
            this.token = loginData.token;
            this.commonService.initGaia(this.token).subscribe((/**
             * @param {?} loaded
             * @return {?}
             */
            loaded => {
                console.log('Gaia init');
            }), (/**
             * @param {?} err
             * @return {?}
             */
            err => console.log(err)));
        }), (/**
         * @param {?} err
         * @return {?}
         */
        err => console.log(err)));
    }
    /**
     * @return {?}
     */
    checkPostalCode() {
        this.commonService.checkPostalCode(this.token, this.postal_code).subscribe((/**
         * @param {?} data
         * @return {?}
         */
        data => {
            this.postal_code_info.emit(data.data);
        }), (/**
         * @param {?} err
         * @return {?}
         */
        err => console.log(err)));
    }
}
CpInputComponent.decorators = [
    { type: Component, args: [{
                selector: 'pcrf-cp-input',
                template: "<pcrf-input-text\n  [placeholder]=\"placeholder\"\n  [(value)]=\"postal_code\"\n  (valueChange)=\"checkPostalCode()\"\n></pcrf-input-text>\n",
                styles: [""]
            }] }
];
/** @nocollapse */
CpInputComponent.ctorParameters = () => [
    { type: CommonService }
];
CpInputComponent.propDecorators = {
    postal_code: [{ type: Input }],
    placeholder: [{ type: Input }],
    postal_code_info: [{ type: Output }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class CpWithButtonComponent {
    constructor() {
        this.button_clicked = new EventEmitter();
        this.disabled = true;
    }
    /**
     * @return {?}
     */
    ngOnInit() { }
    /**
     * @param {?} event
     * @return {?}
     */
    postalCodeChecked(event) {
        console.log(`postalCodeChecked: ${event}`);
        this.disabled = !event;
        this.province = event;
    }
    /**
     * @param {?} event
     * @return {?}
     */
    buttonCLicked(event) {
        console.log(`buttonCLicked: ${event}`);
        this.button_clicked.emit(this.province);
    }
}
CpWithButtonComponent.decorators = [
    { type: Component, args: [{
                selector: 'pcrf-cp-with-button',
                template: "<pcrf-cp-input\n  [postal_code]=\"postal_code\"\n  [placeholder]=\"placeholder\"\n  (postal_code_info)=\"postalCodeChecked($event)\"\n></pcrf-cp-input>\n\n<pcrf-red-button\n  [disabled]=\"disabled\"\n  (event)=\"buttonCLicked($event)\"\n  [text]=\"text_button\"\n></pcrf-red-button>\n",
                styles: [""]
            }] }
];
/** @nocollapse */
CpWithButtonComponent.ctorParameters = () => [];
CpWithButtonComponent.propDecorators = {
    postal_code: [{ type: Input }],
    placeholder: [{ type: Input }],
    button_clicked: [{ type: Output }],
    text_button: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class PcrfCommonModule {
}
PcrfCommonModule.decorators = [
    { type: NgModule, args: [{
                declarations: [
                    HeaderComponent,
                    FooterComponent,
                    InputTextComponent,
                    ImageBoxComponent,
                    TextBoxComponent,
                    RedButtonComponent,
                    CpInputComponent,
                    CpWithButtonComponent
                ],
                imports: [
                    CommonModule,
                    MatButtonModule,
                    MatInputModule,
                    MatCardModule,
                    MatIconModule,
                    HttpClientModule
                ],
                exports: [
                    HeaderComponent,
                    FooterComponent,
                    InputTextComponent,
                    ImageBoxComponent,
                    TextBoxComponent,
                    RedButtonComponent,
                    CpInputComponent,
                    CpWithButtonComponent
                ]
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { PcrfCommonModule, HeaderComponent, FooterComponent, ImageBoxComponent, InputTextComponent, CpInputComponent, CpWithButtonComponent, RedButtonComponent, TextBoxComponent, CommonService };

//# sourceMappingURL=pcrf-common.js.map