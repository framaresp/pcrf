/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { CommonService } from './../common.service';
import { Component, Input, Output, EventEmitter } from '@angular/core';
export class CpInputComponent {
    /**
     * @param {?} commonService
     */
    constructor(commonService) {
        this.commonService = commonService;
        this.postal_code_info = new EventEmitter();
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.commonService.login().subscribe((/**
         * @param {?} loginData
         * @return {?}
         */
        loginData => {
            this.token = loginData.token;
            this.commonService.initGaia(this.token).subscribe((/**
             * @param {?} loaded
             * @return {?}
             */
            loaded => {
                console.log('Gaia init');
            }), (/**
             * @param {?} err
             * @return {?}
             */
            err => console.log(err)));
        }), (/**
         * @param {?} err
         * @return {?}
         */
        err => console.log(err)));
    }
    /**
     * @return {?}
     */
    checkPostalCode() {
        this.commonService.checkPostalCode(this.token, this.postal_code).subscribe((/**
         * @param {?} data
         * @return {?}
         */
        data => {
            this.postal_code_info.emit(data.data);
        }), (/**
         * @param {?} err
         * @return {?}
         */
        err => console.log(err)));
    }
}
CpInputComponent.decorators = [
    { type: Component, args: [{
                selector: 'pcrf-cp-input',
                template: "<pcrf-input-text\n  [placeholder]=\"placeholder\"\n  [(value)]=\"postal_code\"\n  (valueChange)=\"checkPostalCode()\"\n></pcrf-input-text>\n",
                styles: [""]
            }] }
];
/** @nocollapse */
CpInputComponent.ctorParameters = () => [
    { type: CommonService }
];
CpInputComponent.propDecorators = {
    postal_code: [{ type: Input }],
    placeholder: [{ type: Input }],
    postal_code_info: [{ type: Output }]
};
if (false) {
    /** @type {?} */
    CpInputComponent.prototype.postal_code;
    /** @type {?} */
    CpInputComponent.prototype.placeholder;
    /** @type {?} */
    CpInputComponent.prototype.postal_code_info;
    /**
     * @type {?}
     * @private
     */
    CpInputComponent.prototype.token;
    /**
     * @type {?}
     * @private
     */
    CpInputComponent.prototype.commonService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3AtaW5wdXQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQHBjcmYvY29tbW9uLyIsInNvdXJjZXMiOlsibGliL2NwLWlucHV0L2NwLWlucHV0LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBQ3BELE9BQU8sRUFBRSxTQUFTLEVBQVUsS0FBSyxFQUFFLE1BQU0sRUFBRSxZQUFZLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFPL0UsTUFBTSxPQUFPLGdCQUFnQjs7OztJQU8zQixZQUFvQixhQUE0QjtRQUE1QixrQkFBYSxHQUFiLGFBQWEsQ0FBZTtRQUp0QyxxQkFBZ0IsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO0lBSUYsQ0FBQzs7OztJQUVwRCxRQUFRO1FBQ04sSUFBSSxDQUFDLGFBQWEsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxTQUFTOzs7O1FBQ2xDLFNBQVMsQ0FBQyxFQUFFO1lBQ1YsSUFBSSxDQUFDLEtBQUssR0FBRyxTQUFTLENBQUMsS0FBSyxDQUFDO1lBQzdCLElBQUksQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxTQUFTOzs7O1lBQy9DLE1BQU0sQ0FBQyxFQUFFO2dCQUNQLE9BQU8sQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDM0IsQ0FBQzs7OztZQUNELEdBQUcsQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsRUFDeEIsQ0FBQztRQUNKLENBQUM7Ozs7UUFDRCxHQUFHLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEVBQ3hCLENBQUM7SUFDSixDQUFDOzs7O0lBRUQsZUFBZTtRQUNiLElBQUksQ0FBQyxhQUFhLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLFNBQVM7Ozs7UUFDeEUsSUFBSSxDQUFDLEVBQUU7WUFDTCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN4QyxDQUFDOzs7O1FBQ0QsR0FBRyxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxFQUN4QixDQUFDO0lBQ0osQ0FBQzs7O1lBcENGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsZUFBZTtnQkFDekIsd0pBQXdDOzthQUV6Qzs7OztZQVBRLGFBQWE7OzswQkFTbkIsS0FBSzswQkFDTCxLQUFLOytCQUNMLE1BQU07Ozs7SUFGUCx1Q0FBNkI7O0lBQzdCLHVDQUE2Qjs7SUFDN0IsNENBQXFEOzs7OztJQUVyRCxpQ0FBc0I7Ozs7O0lBRVYseUNBQW9DIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tbW9uU2VydmljZSB9IGZyb20gJy4vLi4vY29tbW9uLnNlcnZpY2UnO1xuaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIElucHV0LCBPdXRwdXQsIEV2ZW50RW1pdHRlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdwY3JmLWNwLWlucHV0JyxcbiAgdGVtcGxhdGVVcmw6ICcuL2NwLWlucHV0LmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vY3AtaW5wdXQuY29tcG9uZW50LnNjc3MnXVxufSlcbmV4cG9ydCBjbGFzcyBDcElucHV0Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgQElucHV0KCkgcG9zdGFsX2NvZGU6IHN0cmluZztcbiAgQElucHV0KCkgcGxhY2Vob2xkZXI6IHN0cmluZztcbiAgQE91dHB1dCgpIHBvc3RhbF9jb2RlX2luZm8gPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcblxuICBwcml2YXRlIHRva2VuOiBzdHJpbmc7XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSBjb21tb25TZXJ2aWNlOiBDb21tb25TZXJ2aWNlKSB7fVxuXG4gIG5nT25Jbml0KCkge1xuICAgIHRoaXMuY29tbW9uU2VydmljZS5sb2dpbigpLnN1YnNjcmliZShcbiAgICAgIGxvZ2luRGF0YSA9PiB7XG4gICAgICAgIHRoaXMudG9rZW4gPSBsb2dpbkRhdGEudG9rZW47XG4gICAgICAgIHRoaXMuY29tbW9uU2VydmljZS5pbml0R2FpYSh0aGlzLnRva2VuKS5zdWJzY3JpYmUoXG4gICAgICAgICAgbG9hZGVkID0+IHtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKCdHYWlhIGluaXQnKTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIGVyciA9PiBjb25zb2xlLmxvZyhlcnIpXG4gICAgICAgICk7XG4gICAgICB9LFxuICAgICAgZXJyID0+IGNvbnNvbGUubG9nKGVycilcbiAgICApO1xuICB9XG5cbiAgY2hlY2tQb3N0YWxDb2RlKCk6IHZvaWQge1xuICAgIHRoaXMuY29tbW9uU2VydmljZS5jaGVja1Bvc3RhbENvZGUodGhpcy50b2tlbiwgdGhpcy5wb3N0YWxfY29kZSkuc3Vic2NyaWJlKFxuICAgICAgZGF0YSA9PiB7XG4gICAgICAgIHRoaXMucG9zdGFsX2NvZGVfaW5mby5lbWl0KGRhdGEuZGF0YSk7XG4gICAgICB9LFxuICAgICAgZXJyID0+IGNvbnNvbGUubG9nKGVycilcbiAgICApO1xuICB9XG59XG4iXX0=