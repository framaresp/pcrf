/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
export class CommonService {
    /**
     * @param {?} httpClient
     */
    constructor(httpClient) {
        this.httpClient = httpClient;
        this.httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            })
        };
    }
    /**
     * @param {?} token
     * @param {?} postalCode
     * @return {?}
     */
    checkPostalCode(token, postalCode) {
        return this.httpClient.get(`https://apisb.mapfre.com/srv/pocTarificadorVidaComeRound/components/mfcDeyde/codigoPostal`, {
            headers: new HttpHeaders({
                Authorization: `Bearer ${token}`
            }),
            params: new HttpParams().set('parameters', `{"cp":"${postalCode}","validatePostalCode":false}`),
            withCredentials: true
        });
    }
    /**
     * @return {?}
     */
    login() {
        return this.httpClient.post('https://apisb.mapfre.com/srv/pocTarificadorVidaComeRound/login', {}, {
            headers: new HttpHeaders({
                Authorization: 'Basic QVJDVVNBSTpNYXBmcmUyMDE4'
            })
        });
    }
    /**
     * @param {?} token
     * @return {?}
     */
    initGaia(token) {
        return this.httpClient.post('https://apisb.mapfre.com/srv/pocTarificadorVidaComeRound/register/globaldata', {
            data: {
                config: {
                    real: true,
                    product: 'Vida',
                    branch: 'Vida',
                    entity: 'Vida',
                    group: 'Vida',
                    geo: 'espaÃ±a',
                    core: 'Vida',
                    language: 'es-es',
                    currency: 'â¬',
                    dateFormat: '',
                    direction: false,
                    theme: 'Classic Left',
                    rv: '',
                    helpIcon: 'mfc-icon--info',
                    enterprise: 'Vida',
                    version: 'v4.12.3',
                    analytics: {
                        viewState: 'stepView',
                        viewShadowBox: 'popupView',
                        sendForm: 'formLink'
                    },
                    itemsPromotion: '1',
                    aggregators: ['confu'],
                    dataStackQuote: `lOJ14enR3T5G5/0EUXy2ke14T+vk99QWV7EBlPyt8RtWqLhJzcbouFNLMxxyjs81k89JHRGvMop2Y9SKjx1C5SLm0vzqc1cLboFEyse
              'auBWwbHXrtQgftGU6I5O5aslWGDQaPKVzpQqCEmXoKfEPlCvZyiULbHuF26gYLo4LMhjpPYnCTZlv+aRsjde6/aNFjKciQxd4pQg=`,
                    dataStackBuy: `8NRR4c21peCL37o6o8mBwMIrGj4FAYgFU3eceIKK97n7pIbxtqyMUO8AVC1j4e2/6jzZ2QK7vcUiNXIB/UTgl/hBfvHB+s53HZX7ua6M
              JPe90o3N6cLEaU3nv/HjLvEXADbTE9OpyfoidoD97Hfta0zeSB0k3yhDI6E12oIsMO6J5rRtXQd5Rq6mVF18CFYSQCb8sZIClrxZxys4j
              EI1OYTygUc+HUHWQpkN/5fGqIw5BImBH27ux9xXSfRXjuXzrQheTKx4qYQWhBaze6VqOnOrfJo6dmcPuU1Tsz7UpCzd1v5IiOa0T4YBbx
              YypXhDzgm44cFJseQeMNYL/tT+5W57Gh/hx0le4mTS6wpzZhZ/CVdrZTsZCBkl7HNrlBss9LFCYufTG6nOlUH8p17Re/8xmZu0+8/3DSM
              xoXEBBl2VFCRjpyfh8bdojaBSU/2LvUV8Xbc9e/q+9lyFmHq2soZTqMaWDuKa7gznO+JjBNzRS3/TUiWZUz8cgBbruTp5S1J89PbXhe0r
              iPUYZ3jYxez0dUW+I3V9WVsS1vqXrl8J8cXSS0EKgkTR5hUgejSwBMtWQPD7R7FOSqzt5UIV9+rKVnFI33Mm7e/O/ex09HwtuU/3KmNBL
              BsD9f/HCPtwiT+sSkA/Awfh6X2FNHC0KPVq9PNTTrgVkZ2iIY5DVa/khum8t0bGHYIxHJvXY+z+OUKKljwK5wnp0BMhzYRf47TGCA9w6q
              gv1m5+Ep2q5w2da0oqy6Gbxp7yqV3MXbnqKIiFfAquryA=`,
                    exitApp: {}
                },
                browser: {
                    platform: 'Windows',
                    navigator: {
                        navigatorType: 'Chrome',
                        navigatorVersion: '74.0.3729.108'
                    },
                    mobile: false
                }
            }
        }, {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                Authorization: `Bearer ${token}`
            }),
            withCredentials: true
        });
    }
    /**
     * @param {?} page_id
     * @return {?}
     */
    getTridionData(page_id) {
        return this.httpClient.get(`https://poc-api-mapfre.ciberexperis.net/api/PageContents/${page_id}`, this.httpOptions);
    }
    /**
     * @param {?} com_id
     * @return {?}
     */
    getTridionComponent(com_id) {
        com_id = com_id.replace('tcm:', '');
        return this.httpClient.get(`https://poc-api-mapfre.ciberexperis.net/api/Components/${com_id}`, this.httpOptions);
    }
    /**
     * @param {?} pub_id
     * @return {?}
     */
    getPublicationData(pub_id) {
        return this.httpClient.get(`https://poc-api-mapfre.ciberexperis.net/api/PublicationPageContents/${pub_id}`, this.httpOptions);
    }
    /**
     * @param {?} device
     * @return {?}
     */
    getSmartTarget(device) {
        return this.httpClient.post('https://poc-smarttarget-mapfre.ciberexperis.net/xo-promotions/_search?pretty', {
            query: {
                bool: {
                    must: [
                        { match: { scopePublication: 'tcm:0-3-1' } },
                        {
                            match: {
                                'triggers.triggerValues.stringValues': device
                            }
                        },
                        { match: { 'triggers.triggerValues.longValues': 114 } },
                        { match: { state: 'PENDING_ACTIVATION' } }
                    ]
                }
            }
        }, this.httpOptions);
    }
    // Create content in tridion
    /**
     * @param {?} publication_id
     * @param {?} page_title
     * @param {?} file_name
     * @param {?} pink_folder_id
     * @param {?} page_template_id
     * @return {?}
     */
    createPageInTridion(publication_id, page_title, file_name, pink_folder_id, page_template_id) {
        /** @type {?} */
        const body = {
            Id: 'tcm:0-0-0',
            Title: page_title,
            FileName: file_name,
            LocationInfo: {
                PublishLocationPath: '',
                PublishLocationUrl: '',
                PublishPath: '',
                ContextRepository: {
                    IdRef: publication_id,
                    Title: ''
                },
                OrganizationalItem: {
                    IdRef: pink_folder_id,
                    Title: ''
                },
                Path: '',
                WebDavUrl: ''
            },
            PageTemplate: {
                IdRef: page_template_id,
                Title: ''
            }
        };
        return this.httpClient.post('https://poc-api-mapfre.ciberexperis.net/api/Page', body, this.httpOptions);
    }
    /**
     * @param {?} yellow_folder_id
     * @param {?} schema_id
     * @param {?} title
     * @param {?} content
     * @return {?}
     */
    createComponentToPageInTridion(yellow_folder_id, schema_id, title, content) {
        /** @type {?} */
        const body = {
            Id: 'tcm:0-0-0',
            SchemaID: schema_id,
            Title: title,
            FolderID: yellow_folder_id,
            Content: content
        };
        return this.httpClient.post('https://poc-api-mapfre.ciberexperis.net/api/ComponentContents', body, this.httpOptions);
    }
    /**
     * @param {?} page_id
     * @param {?} components
     * @return {?}
     */
    addComponentToPageInTridion(page_id, components) {
        /** @type {?} */
        const body = {
            PageId: page_id,
            ComponentPresentations: components
        };
        return this.httpClient.put('https://poc-api-mapfre.ciberexperis.net/api/Page', body, this.httpOptions);
    }
}
CommonService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
CommonService.ctorParameters = () => [
    { type: HttpClient }
];
/** @nocollapse */ CommonService.ngInjectableDef = i0.defineInjectable({ factory: function CommonService_Factory() { return new CommonService(i0.inject(i1.HttpClient)); }, token: CommonService, providedIn: "root" });
if (false) {
    /** @type {?} */
    CommonService.prototype.httpOptions;
    /**
     * @type {?}
     * @private
     */
    CommonService.prototype.httpClient;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tbW9uLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AcGNyZi9jb21tb24vIiwic291cmNlcyI6WyJsaWIvY29tbW9uLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLFVBQVUsRUFBRSxXQUFXLEVBQUUsVUFBVSxFQUFFLE1BQU0sc0JBQXNCLENBQUM7OztBQU0zRSxNQUFNLE9BQU8sYUFBYTs7OztJQU94QixZQUFvQixVQUFzQjtRQUF0QixlQUFVLEdBQVYsVUFBVSxDQUFZO1FBTjFDLGdCQUFXLEdBQUc7WUFDWixPQUFPLEVBQUUsSUFBSSxXQUFXLENBQUM7Z0JBQ3ZCLGNBQWMsRUFBRSxrQkFBa0I7YUFDbkMsQ0FBQztTQUNILENBQUM7SUFFMkMsQ0FBQzs7Ozs7O0lBRTlDLGVBQWUsQ0FBQyxLQUFhLEVBQUUsVUFBa0I7UUFDL0MsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FDeEIsMkZBQTJGLEVBQzNGO1lBQ0UsT0FBTyxFQUFFLElBQUksV0FBVyxDQUFDO2dCQUN2QixhQUFhLEVBQUUsVUFBVSxLQUFLLEVBQUU7YUFDakMsQ0FBQztZQUNGLE1BQU0sRUFBRSxJQUFJLFVBQVUsRUFBRSxDQUFDLEdBQUcsQ0FDMUIsWUFBWSxFQUNaLFVBQVUsVUFBVSwrQkFBK0IsQ0FDcEQ7WUFDRCxlQUFlLEVBQUUsSUFBSTtTQUN0QixDQUNGLENBQUM7SUFDSixDQUFDOzs7O0lBRUQsS0FBSztRQUNILE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQ3pCLGdFQUFnRSxFQUNoRSxFQUFFLEVBQ0Y7WUFDRSxPQUFPLEVBQUUsSUFBSSxXQUFXLENBQUM7Z0JBQ3ZCLGFBQWEsRUFBRSxnQ0FBZ0M7YUFDaEQsQ0FBQztTQUNILENBQ0YsQ0FBQztJQUNKLENBQUM7Ozs7O0lBRUQsUUFBUSxDQUFDLEtBQWE7UUFDcEIsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FDekIsOEVBQThFLEVBQzlFO1lBQ0UsSUFBSSxFQUFFO2dCQUNKLE1BQU0sRUFBRTtvQkFDTixJQUFJLEVBQUUsSUFBSTtvQkFDVixPQUFPLEVBQUUsTUFBTTtvQkFDZixNQUFNLEVBQUUsTUFBTTtvQkFDZCxNQUFNLEVBQUUsTUFBTTtvQkFDZCxLQUFLLEVBQUUsTUFBTTtvQkFDYixHQUFHLEVBQUUsU0FBUztvQkFDZCxJQUFJLEVBQUUsTUFBTTtvQkFDWixRQUFRLEVBQUUsT0FBTztvQkFDakIsUUFBUSxFQUFFLEtBQUs7b0JBQ2YsVUFBVSxFQUFFLEVBQUU7b0JBQ2QsU0FBUyxFQUFFLEtBQUs7b0JBQ2hCLEtBQUssRUFBRSxjQUFjO29CQUNyQixFQUFFLEVBQUUsRUFBRTtvQkFDTixRQUFRLEVBQUUsZ0JBQWdCO29CQUMxQixVQUFVLEVBQUUsTUFBTTtvQkFDbEIsT0FBTyxFQUFFLFNBQVM7b0JBQ2xCLFNBQVMsRUFBRTt3QkFDVCxTQUFTLEVBQUUsVUFBVTt3QkFDckIsYUFBYSxFQUFFLFdBQVc7d0JBQzFCLFFBQVEsRUFBRSxVQUFVO3FCQUNyQjtvQkFDRCxjQUFjLEVBQUUsR0FBRztvQkFDbkIsV0FBVyxFQUFFLENBQUMsT0FBTyxDQUFDO29CQUN0QixjQUFjLEVBQUU7cUhBQ3lGO29CQUN6RyxZQUFZLEVBQUU7Ozs7Ozs7NkRBT21DO29CQUNqRCxPQUFPLEVBQUUsRUFBRTtpQkFDWjtnQkFDRCxPQUFPLEVBQUU7b0JBQ1AsUUFBUSxFQUFFLFNBQVM7b0JBQ25CLFNBQVMsRUFBRTt3QkFDVCxhQUFhLEVBQUUsUUFBUTt3QkFDdkIsZ0JBQWdCLEVBQUUsZUFBZTtxQkFDbEM7b0JBQ0QsTUFBTSxFQUFFLEtBQUs7aUJBQ2Q7YUFDRjtTQUNGLEVBQ0Q7WUFDRSxPQUFPLEVBQUUsSUFBSSxXQUFXLENBQUM7Z0JBQ3ZCLGNBQWMsRUFBRSxrQkFBa0I7Z0JBQ2xDLGFBQWEsRUFBRSxVQUFVLEtBQUssRUFBRTthQUNqQyxDQUFDO1lBQ0YsZUFBZSxFQUFFLElBQUk7U0FDdEIsQ0FDRixDQUFDO0lBQ0osQ0FBQzs7Ozs7SUFFRCxjQUFjLENBQUMsT0FBZTtRQUM1QixPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUN4Qiw0REFBNEQsT0FBTyxFQUFFLEVBQ3JFLElBQUksQ0FBQyxXQUFXLENBQ2pCLENBQUM7SUFDSixDQUFDOzs7OztJQUVELG1CQUFtQixDQUFDLE1BQWM7UUFDaEMsTUFBTSxHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFLEVBQUUsQ0FBQyxDQUFDO1FBQ3BDLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQ3hCLDBEQUEwRCxNQUFNLEVBQUUsRUFDbEUsSUFBSSxDQUFDLFdBQVcsQ0FDakIsQ0FBQztJQUNKLENBQUM7Ozs7O0lBRUQsa0JBQWtCLENBQUMsTUFBTTtRQUN2QixPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUN4Qix1RUFBdUUsTUFBTSxFQUFFLEVBQy9FLElBQUksQ0FBQyxXQUFXLENBQ2pCLENBQUM7SUFDSixDQUFDOzs7OztJQUVELGNBQWMsQ0FBQyxNQUFjO1FBQzNCLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQ3pCLDhFQUE4RSxFQUM5RTtZQUNFLEtBQUssRUFBRTtnQkFDTCxJQUFJLEVBQUU7b0JBQ0osSUFBSSxFQUFFO3dCQUNKLEVBQUUsS0FBSyxFQUFFLEVBQUUsZ0JBQWdCLEVBQUUsV0FBVyxFQUFFLEVBQUU7d0JBQzVDOzRCQUNFLEtBQUssRUFBRTtnQ0FDTCxxQ0FBcUMsRUFBRSxNQUFNOzZCQUM5Qzt5QkFDRjt3QkFDRCxFQUFFLEtBQUssRUFBRSxFQUFFLG1DQUFtQyxFQUFFLEdBQUcsRUFBRSxFQUFFO3dCQUN2RCxFQUFFLEtBQUssRUFBRSxFQUFFLEtBQUssRUFBRSxvQkFBb0IsRUFBRSxFQUFFO3FCQUMzQztpQkFDRjthQUNGO1NBQ0YsRUFDRCxJQUFJLENBQUMsV0FBVyxDQUNqQixDQUFDO0lBQ0osQ0FBQzs7Ozs7Ozs7OztJQUdELG1CQUFtQixDQUNqQixjQUFzQixFQUN0QixVQUFrQixFQUNsQixTQUFpQixFQUNqQixjQUFzQixFQUN0QixnQkFBd0I7O2NBRWxCLElBQUksR0FBRztZQUNYLEVBQUUsRUFBRSxXQUFXO1lBQ2YsS0FBSyxFQUFFLFVBQVU7WUFDakIsUUFBUSxFQUFFLFNBQVM7WUFDbkIsWUFBWSxFQUFFO2dCQUNaLG1CQUFtQixFQUFFLEVBQUU7Z0JBQ3ZCLGtCQUFrQixFQUFFLEVBQUU7Z0JBQ3RCLFdBQVcsRUFBRSxFQUFFO2dCQUNmLGlCQUFpQixFQUFFO29CQUNqQixLQUFLLEVBQUUsY0FBYztvQkFDckIsS0FBSyxFQUFFLEVBQUU7aUJBQ1Y7Z0JBQ0Qsa0JBQWtCLEVBQUU7b0JBQ2xCLEtBQUssRUFBRSxjQUFjO29CQUNyQixLQUFLLEVBQUUsRUFBRTtpQkFDVjtnQkFDRCxJQUFJLEVBQUUsRUFBRTtnQkFDUixTQUFTLEVBQUUsRUFBRTthQUNkO1lBQ0QsWUFBWSxFQUFFO2dCQUNaLEtBQUssRUFBRSxnQkFBZ0I7Z0JBQ3ZCLEtBQUssRUFBRSxFQUFFO2FBQ1Y7U0FDRjtRQUVELE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQ3pCLGtEQUFrRCxFQUNsRCxJQUFJLEVBQ0osSUFBSSxDQUFDLFdBQVcsQ0FDakIsQ0FBQztJQUNKLENBQUM7Ozs7Ozs7O0lBRUQsOEJBQThCLENBQzVCLGdCQUF3QixFQUN4QixTQUFpQixFQUNqQixLQUFhLEVBQ2IsT0FBWTs7Y0FFTixJQUFJLEdBQUc7WUFDWCxFQUFFLEVBQUUsV0FBVztZQUNmLFFBQVEsRUFBRSxTQUFTO1lBQ25CLEtBQUssRUFBRSxLQUFLO1lBQ1osUUFBUSxFQUFFLGdCQUFnQjtZQUMxQixPQUFPLEVBQUUsT0FBTztTQUNqQjtRQUVELE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQ3pCLCtEQUErRCxFQUMvRCxJQUFJLEVBQ0osSUFBSSxDQUFDLFdBQVcsQ0FDakIsQ0FBQztJQUNKLENBQUM7Ozs7OztJQUVELDJCQUEyQixDQUN6QixPQUFlLEVBQ2YsVUFBZTs7Y0FFVCxJQUFJLEdBQUc7WUFDWCxNQUFNLEVBQUUsT0FBTztZQUNmLHNCQUFzQixFQUFFLFVBQVU7U0FDbkM7UUFFRCxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUN4QixrREFBa0QsRUFDbEQsSUFBSSxFQUNKLElBQUksQ0FBQyxXQUFXLENBQ2pCLENBQUM7SUFDSixDQUFDOzs7WUE3TkYsVUFBVSxTQUFDO2dCQUNWLFVBQVUsRUFBRSxNQUFNO2FBQ25COzs7O1lBTFEsVUFBVTs7Ozs7SUFPakIsb0NBSUU7Ozs7O0lBRVUsbUNBQThCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgSHR0cENsaWVudCwgSHR0cEhlYWRlcnMsIEh0dHBQYXJhbXMgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XG5cbkBJbmplY3RhYmxlKHtcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuZXhwb3J0IGNsYXNzIENvbW1vblNlcnZpY2Uge1xuICBodHRwT3B0aW9ucyA9IHtcbiAgICBoZWFkZXJzOiBuZXcgSHR0cEhlYWRlcnMoe1xuICAgICAgJ0NvbnRlbnQtVHlwZSc6ICdhcHBsaWNhdGlvbi9qc29uJ1xuICAgIH0pXG4gIH07XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSBodHRwQ2xpZW50OiBIdHRwQ2xpZW50KSB7fVxuXG4gIGNoZWNrUG9zdGFsQ29kZSh0b2tlbjogc3RyaW5nLCBwb3N0YWxDb2RlOiBzdHJpbmcpOiBPYnNlcnZhYmxlPGFueT4ge1xuICAgIHJldHVybiB0aGlzLmh0dHBDbGllbnQuZ2V0KFxuICAgICAgYGh0dHBzOi8vYXBpc2IubWFwZnJlLmNvbS9zcnYvcG9jVGFyaWZpY2Fkb3JWaWRhQ29tZVJvdW5kL2NvbXBvbmVudHMvbWZjRGV5ZGUvY29kaWdvUG9zdGFsYCxcbiAgICAgIHtcbiAgICAgICAgaGVhZGVyczogbmV3IEh0dHBIZWFkZXJzKHtcbiAgICAgICAgICBBdXRob3JpemF0aW9uOiBgQmVhcmVyICR7dG9rZW59YFxuICAgICAgICB9KSxcbiAgICAgICAgcGFyYW1zOiBuZXcgSHR0cFBhcmFtcygpLnNldChcbiAgICAgICAgICAncGFyYW1ldGVycycsXG4gICAgICAgICAgYHtcImNwXCI6XCIke3Bvc3RhbENvZGV9XCIsXCJ2YWxpZGF0ZVBvc3RhbENvZGVcIjpmYWxzZX1gXG4gICAgICAgICksXG4gICAgICAgIHdpdGhDcmVkZW50aWFsczogdHJ1ZVxuICAgICAgfVxuICAgICk7XG4gIH1cblxuICBsb2dpbigpOiBPYnNlcnZhYmxlPGFueT4ge1xuICAgIHJldHVybiB0aGlzLmh0dHBDbGllbnQucG9zdChcbiAgICAgICdodHRwczovL2FwaXNiLm1hcGZyZS5jb20vc3J2L3BvY1RhcmlmaWNhZG9yVmlkYUNvbWVSb3VuZC9sb2dpbicsXG4gICAgICB7fSxcbiAgICAgIHtcbiAgICAgICAgaGVhZGVyczogbmV3IEh0dHBIZWFkZXJzKHtcbiAgICAgICAgICBBdXRob3JpemF0aW9uOiAnQmFzaWMgUVZKRFZWTkJTVHBOWVhCbWNtVXlNREU0J1xuICAgICAgICB9KVxuICAgICAgfVxuICAgICk7XG4gIH1cblxuICBpbml0R2FpYSh0b2tlbjogc3RyaW5nKTogT2JzZXJ2YWJsZTxhbnk+IHtcbiAgICByZXR1cm4gdGhpcy5odHRwQ2xpZW50LnBvc3QoXG4gICAgICAnaHR0cHM6Ly9hcGlzYi5tYXBmcmUuY29tL3Nydi9wb2NUYXJpZmljYWRvclZpZGFDb21lUm91bmQvcmVnaXN0ZXIvZ2xvYmFsZGF0YScsXG4gICAgICB7XG4gICAgICAgIGRhdGE6IHtcbiAgICAgICAgICBjb25maWc6IHtcbiAgICAgICAgICAgIHJlYWw6IHRydWUsXG4gICAgICAgICAgICBwcm9kdWN0OiAnVmlkYScsXG4gICAgICAgICAgICBicmFuY2g6ICdWaWRhJyxcbiAgICAgICAgICAgIGVudGl0eTogJ1ZpZGEnLFxuICAgICAgICAgICAgZ3JvdXA6ICdWaWRhJyxcbiAgICAgICAgICAgIGdlbzogJ2VzcGHDg8KxYScsXG4gICAgICAgICAgICBjb3JlOiAnVmlkYScsXG4gICAgICAgICAgICBsYW5ndWFnZTogJ2VzLWVzJyxcbiAgICAgICAgICAgIGN1cnJlbmN5OiAnw6LCgsKsJyxcbiAgICAgICAgICAgIGRhdGVGb3JtYXQ6ICcnLFxuICAgICAgICAgICAgZGlyZWN0aW9uOiBmYWxzZSxcbiAgICAgICAgICAgIHRoZW1lOiAnQ2xhc3NpYyBMZWZ0JyxcbiAgICAgICAgICAgIHJ2OiAnJyxcbiAgICAgICAgICAgIGhlbHBJY29uOiAnbWZjLWljb24tLWluZm8nLFxuICAgICAgICAgICAgZW50ZXJwcmlzZTogJ1ZpZGEnLFxuICAgICAgICAgICAgdmVyc2lvbjogJ3Y0LjEyLjMnLFxuICAgICAgICAgICAgYW5hbHl0aWNzOiB7XG4gICAgICAgICAgICAgIHZpZXdTdGF0ZTogJ3N0ZXBWaWV3JyxcbiAgICAgICAgICAgICAgdmlld1NoYWRvd0JveDogJ3BvcHVwVmlldycsXG4gICAgICAgICAgICAgIHNlbmRGb3JtOiAnZm9ybUxpbmsnXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgaXRlbXNQcm9tb3Rpb246ICcxJyxcbiAgICAgICAgICAgIGFnZ3JlZ2F0b3JzOiBbJ2NvbmZ1J10sXG4gICAgICAgICAgICBkYXRhU3RhY2tRdW90ZTogYGxPSjE0ZW5SM1Q1RzUvMEVVWHkya2UxNFQrdms5OVFXVjdFQmxQeXQ4UnRXcUxoSnpjYm91Rk5MTXh4eWpzODFrODlKSFJHdk1vcDJZOVNLangxQzVTTG0wdnpxYzFjTGJvRkV5c2VcbiAgICAgICAgICAgICAgJ2F1Qld3YkhYcnRRZ2Z0R1U2STVPNWFzbFdHRFFhUEtWenBRcUNFbVhvS2ZFUGxDdlp5aVVMYkh1RjI2Z1lMbzRMTWhqcFBZbkNUWmx2K2FSc2pkZTYvYU5GaktjaVF4ZDRwUWc9YCxcbiAgICAgICAgICAgIGRhdGFTdGFja0J1eTogYDhOUlI0YzIxcGVDTDM3bzZvOG1Cd01JckdqNEZBWWdGVTNlY2VJS0s5N243cElieHRxeU1VTzhBVkMxajRlMi82anpaMlFLN3ZjVWlOWElCL1VUZ2wvaEJmdkhCK3M1M0haWDd1YTZNXG4gICAgICAgICAgICAgIEpQZTkwbzNONmNMRWFVM252L0hqTHZFWEFEYlRFOU9weWZvaWRvRDk3SGZ0YTB6ZVNCMGszeWhESTZFMTJvSXNNTzZKNXJSdFhRZDVScTZtVkYxOENGWVNRQ2I4c1pJQ2xyeFp4eXM0alxuICAgICAgICAgICAgICBFSTFPWVR5Z1VjK0hVSFdRcGtOLzVmR3FJdzVCSW1CSDI3dXg5eFhTZlJYanVYenJRaGVUS3g0cVlRV2hCYXplNlZxT25PcmZKbzZkbWNQdVUxVHN6N1VwQ3pkMXY1SWlPYTBUNFlCYnhcbiAgICAgICAgICAgICAgWXlwWGhEemdtNDRjRkpzZVFlTU5ZTC90VCs1VzU3R2gvaHgwbGU0bVRTNndwelpoWi9DVmRyWlRzWkNCa2w3SE5ybEJzczlMRkNZdWZURzZuT2xVSDhwMTdSZS84eG1adTArOC8zRFNNXG4gICAgICAgICAgICAgIHhvWEVCQmwyVkZDUmpweWZoOGJkb2phQlNVLzJMdlVWOFhiYzllL3ErOWx5Rm1IcTJzb1pUcU1hV0R1S2E3Z3puTytKakJOelJTMy9UVWlXWlV6OGNnQmJydVRwNVMxSjg5UGJYaGUwclxuICAgICAgICAgICAgICBpUFVZWjNqWXhlejBkVVcrSTNWOVdWc1MxdnFYcmw4SjhjWFNTMEVLZ2tUUjVoVWdlalN3Qk10V1FQRDdSN0ZPU3F6dDVVSVY5K3JLVm5GSTMzTW03ZS9PL2V4MDlId3R1VS8zS21OQkxcbiAgICAgICAgICAgICAgQnNEOWYvSENQdHdpVCtzU2tBL0F3Zmg2WDJGTkhDMEtQVnE5UE5UVHJnVmtaMmlJWTVEVmEva2h1bTh0MGJHSFlJeEhKdlhZK3orT1VLS2xqd0s1d25wMEJNaHpZUmY0N1RHQ0E5dzZxXG4gICAgICAgICAgICAgIGd2MW01K0VwMnE1dzJkYTBvcXk2R2J4cDd5cVYzTVhibnFLSWlGZkFxdXJ5QT1gLFxuICAgICAgICAgICAgZXhpdEFwcDoge31cbiAgICAgICAgICB9LFxuICAgICAgICAgIGJyb3dzZXI6IHtcbiAgICAgICAgICAgIHBsYXRmb3JtOiAnV2luZG93cycsXG4gICAgICAgICAgICBuYXZpZ2F0b3I6IHtcbiAgICAgICAgICAgICAgbmF2aWdhdG9yVHlwZTogJ0Nocm9tZScsXG4gICAgICAgICAgICAgIG5hdmlnYXRvclZlcnNpb246ICc3NC4wLjM3MjkuMTA4J1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIG1vYmlsZTogZmFsc2VcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIGhlYWRlcnM6IG5ldyBIdHRwSGVhZGVycyh7XG4gICAgICAgICAgJ0NvbnRlbnQtVHlwZSc6ICdhcHBsaWNhdGlvbi9qc29uJyxcbiAgICAgICAgICBBdXRob3JpemF0aW9uOiBgQmVhcmVyICR7dG9rZW59YFxuICAgICAgICB9KSxcbiAgICAgICAgd2l0aENyZWRlbnRpYWxzOiB0cnVlXG4gICAgICB9XG4gICAgKTtcbiAgfVxuXG4gIGdldFRyaWRpb25EYXRhKHBhZ2VfaWQ6IHN0cmluZyk6IE9ic2VydmFibGU8YW55PiB7XG4gICAgcmV0dXJuIHRoaXMuaHR0cENsaWVudC5nZXQoXG4gICAgICBgaHR0cHM6Ly9wb2MtYXBpLW1hcGZyZS5jaWJlcmV4cGVyaXMubmV0L2FwaS9QYWdlQ29udGVudHMvJHtwYWdlX2lkfWAsXG4gICAgICB0aGlzLmh0dHBPcHRpb25zXG4gICAgKTtcbiAgfVxuXG4gIGdldFRyaWRpb25Db21wb25lbnQoY29tX2lkOiBzdHJpbmcpOiBPYnNlcnZhYmxlPGFueT4ge1xuICAgIGNvbV9pZCA9IGNvbV9pZC5yZXBsYWNlKCd0Y206JywgJycpO1xuICAgIHJldHVybiB0aGlzLmh0dHBDbGllbnQuZ2V0KFxuICAgICAgYGh0dHBzOi8vcG9jLWFwaS1tYXBmcmUuY2liZXJleHBlcmlzLm5ldC9hcGkvQ29tcG9uZW50cy8ke2NvbV9pZH1gLFxuICAgICAgdGhpcy5odHRwT3B0aW9uc1xuICAgICk7XG4gIH1cblxuICBnZXRQdWJsaWNhdGlvbkRhdGEocHViX2lkKTogT2JzZXJ2YWJsZTxhbnk+IHtcbiAgICByZXR1cm4gdGhpcy5odHRwQ2xpZW50LmdldChcbiAgICAgIGBodHRwczovL3BvYy1hcGktbWFwZnJlLmNpYmVyZXhwZXJpcy5uZXQvYXBpL1B1YmxpY2F0aW9uUGFnZUNvbnRlbnRzLyR7cHViX2lkfWAsXG4gICAgICB0aGlzLmh0dHBPcHRpb25zXG4gICAgKTtcbiAgfVxuXG4gIGdldFNtYXJ0VGFyZ2V0KGRldmljZTogc3RyaW5nKTogT2JzZXJ2YWJsZTxhbnk+IHtcbiAgICByZXR1cm4gdGhpcy5odHRwQ2xpZW50LnBvc3QoXG4gICAgICAnaHR0cHM6Ly9wb2Mtc21hcnR0YXJnZXQtbWFwZnJlLmNpYmVyZXhwZXJpcy5uZXQveG8tcHJvbW90aW9ucy9fc2VhcmNoP3ByZXR0eScsXG4gICAgICB7XG4gICAgICAgIHF1ZXJ5OiB7XG4gICAgICAgICAgYm9vbDoge1xuICAgICAgICAgICAgbXVzdDogW1xuICAgICAgICAgICAgICB7IG1hdGNoOiB7IHNjb3BlUHVibGljYXRpb246ICd0Y206MC0zLTEnIH0gfSxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIG1hdGNoOiB7XG4gICAgICAgICAgICAgICAgICAndHJpZ2dlcnMudHJpZ2dlclZhbHVlcy5zdHJpbmdWYWx1ZXMnOiBkZXZpY2VcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHsgbWF0Y2g6IHsgJ3RyaWdnZXJzLnRyaWdnZXJWYWx1ZXMubG9uZ1ZhbHVlcyc6IDExNCB9IH0sXG4gICAgICAgICAgICAgIHsgbWF0Y2g6IHsgc3RhdGU6ICdQRU5ESU5HX0FDVElWQVRJT04nIH0gfVxuICAgICAgICAgICAgXVxuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfSxcbiAgICAgIHRoaXMuaHR0cE9wdGlvbnNcbiAgICApO1xuICB9XG5cbiAgLy8gQ3JlYXRlIGNvbnRlbnQgaW4gdHJpZGlvblxuICBjcmVhdGVQYWdlSW5UcmlkaW9uKFxuICAgIHB1YmxpY2F0aW9uX2lkOiBzdHJpbmcsXG4gICAgcGFnZV90aXRsZTogc3RyaW5nLFxuICAgIGZpbGVfbmFtZTogc3RyaW5nLFxuICAgIHBpbmtfZm9sZGVyX2lkOiBzdHJpbmcsXG4gICAgcGFnZV90ZW1wbGF0ZV9pZDogc3RyaW5nXG4gICk6IE9ic2VydmFibGU8YW55PiB7XG4gICAgY29uc3QgYm9keSA9IHtcbiAgICAgIElkOiAndGNtOjAtMC0wJyxcbiAgICAgIFRpdGxlOiBwYWdlX3RpdGxlLFxuICAgICAgRmlsZU5hbWU6IGZpbGVfbmFtZSxcbiAgICAgIExvY2F0aW9uSW5mbzoge1xuICAgICAgICBQdWJsaXNoTG9jYXRpb25QYXRoOiAnJyxcbiAgICAgICAgUHVibGlzaExvY2F0aW9uVXJsOiAnJyxcbiAgICAgICAgUHVibGlzaFBhdGg6ICcnLFxuICAgICAgICBDb250ZXh0UmVwb3NpdG9yeToge1xuICAgICAgICAgIElkUmVmOiBwdWJsaWNhdGlvbl9pZCxcbiAgICAgICAgICBUaXRsZTogJydcbiAgICAgICAgfSxcbiAgICAgICAgT3JnYW5pemF0aW9uYWxJdGVtOiB7XG4gICAgICAgICAgSWRSZWY6IHBpbmtfZm9sZGVyX2lkLFxuICAgICAgICAgIFRpdGxlOiAnJ1xuICAgICAgICB9LFxuICAgICAgICBQYXRoOiAnJyxcbiAgICAgICAgV2ViRGF2VXJsOiAnJ1xuICAgICAgfSxcbiAgICAgIFBhZ2VUZW1wbGF0ZToge1xuICAgICAgICBJZFJlZjogcGFnZV90ZW1wbGF0ZV9pZCxcbiAgICAgICAgVGl0bGU6ICcnXG4gICAgICB9XG4gICAgfTtcblxuICAgIHJldHVybiB0aGlzLmh0dHBDbGllbnQucG9zdChcbiAgICAgICdodHRwczovL3BvYy1hcGktbWFwZnJlLmNpYmVyZXhwZXJpcy5uZXQvYXBpL1BhZ2UnLFxuICAgICAgYm9keSxcbiAgICAgIHRoaXMuaHR0cE9wdGlvbnNcbiAgICApO1xuICB9XG5cbiAgY3JlYXRlQ29tcG9uZW50VG9QYWdlSW5UcmlkaW9uKFxuICAgIHllbGxvd19mb2xkZXJfaWQ6IHN0cmluZyxcbiAgICBzY2hlbWFfaWQ6IHN0cmluZyxcbiAgICB0aXRsZTogc3RyaW5nLFxuICAgIGNvbnRlbnQ6IGFueVxuICApOiBPYnNlcnZhYmxlPGFueT4ge1xuICAgIGNvbnN0IGJvZHkgPSB7XG4gICAgICBJZDogJ3RjbTowLTAtMCcsXG4gICAgICBTY2hlbWFJRDogc2NoZW1hX2lkLFxuICAgICAgVGl0bGU6IHRpdGxlLFxuICAgICAgRm9sZGVySUQ6IHllbGxvd19mb2xkZXJfaWQsXG4gICAgICBDb250ZW50OiBjb250ZW50XG4gICAgfTtcblxuICAgIHJldHVybiB0aGlzLmh0dHBDbGllbnQucG9zdChcbiAgICAgICdodHRwczovL3BvYy1hcGktbWFwZnJlLmNpYmVyZXhwZXJpcy5uZXQvYXBpL0NvbXBvbmVudENvbnRlbnRzJyxcbiAgICAgIGJvZHksXG4gICAgICB0aGlzLmh0dHBPcHRpb25zXG4gICAgKTtcbiAgfVxuXG4gIGFkZENvbXBvbmVudFRvUGFnZUluVHJpZGlvbihcbiAgICBwYWdlX2lkOiBzdHJpbmcsXG4gICAgY29tcG9uZW50czogYW55XG4gICk6IE9ic2VydmFibGU8YW55PiB7XG4gICAgY29uc3QgYm9keSA9IHtcbiAgICAgIFBhZ2VJZDogcGFnZV9pZCxcbiAgICAgIENvbXBvbmVudFByZXNlbnRhdGlvbnM6IGNvbXBvbmVudHNcbiAgICB9O1xuXG4gICAgcmV0dXJuIHRoaXMuaHR0cENsaWVudC5wdXQoXG4gICAgICAnaHR0cHM6Ly9wb2MtYXBpLW1hcGZyZS5jaWJlcmV4cGVyaXMubmV0L2FwaS9QYWdlJyxcbiAgICAgIGJvZHksXG4gICAgICB0aGlzLmh0dHBPcHRpb25zXG4gICAgKTtcbiAgfVxufVxuIl19