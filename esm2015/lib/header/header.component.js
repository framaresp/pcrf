/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input } from '@angular/core';
export class HeaderComponent {
    constructor() { }
    /**
     * @return {?}
     */
    ngOnInit() { }
    /**
     * @param {?} event
     * @return {?}
     */
    loco_clicked(event) {
        this.loco_click.call(event);
    }
    /**
     * @param {?} link
     * @param {?} event
     * @return {?}
     */
    openIconLinks(link, event) {
        link.click.call(event);
    }
}
HeaderComponent.decorators = [
    { type: Component, args: [{
                selector: 'pcrf-header',
                template: "<div class=\"mfc-header__content\">\n  <div class=\"mfc-header__content__main\">\n    <a\n      [title]=\"logo_title\"\n      (click)=\"loco_clicked($event)\"\n      class=\"mfc-header__content__main__link\"\n    >\n      <img class=\"mc-header__content__main__logo\" [src]=\"logo_url\" />\n    </a>\n  </div>\n  <div class=\"mfc-header__content__heading\">\n    <h1 class=\"mfc-header__content__heading__element\">\n      {{ title }}\n    </h1>\n    <h2 *ngIf=\"subtitle\" class=\"mfc-header__content__heading__text\">\n      {{ subtitle }}\n    </h2>\n  </div>\n  <section class=\"mfc-header__content__contact\">\n    <div *ngIf=\"contact_phone\" class=\"mfc-header__content__contact__phone\">\n      <p class=\"mfc-header__content__contact__phone__text\">\n        <mat-icon\n          fontSet=\"fa\"\n          fontIcon=\"fa-phone\"\n          class=\"mfc-header__content__contact__phone__icon\"\n        ></mat-icon>\n        {{ contact_phone }}\n      </p>\n    </div>\n    <ul *ngIf=\"icon_links\" class=\"mfc-header__content__contact__list\">\n      <li\n        *ngFor=\"let link of icon_links\"\n        class=\"mfc-header__content__contact__list__element\"\n      >\n        <a\n          (click)=\"openIconLinks(link, $event)\"\n          class=\"mfc-header__content__link\"\n        >\n          <mat-icon\n            fontSet=\"fa\"\n            [fontIcon]=\"link.icon\"\n            class=\"mfc-icon\"\n          ></mat-icon>\n          <div class=\"mfc-u-inline mfc-header__content__contact__list__text\">\n            {{ link.label }}\n          </div>\n        </a>\n      </li>\n    </ul>\n  </section>\n</div>\n",
                styles: [":host{background:#d81e05;display:block}:host .mfc-header__content{display:flex;align-items:center;flex-wrap:wrap;color:#fff;margin:0 auto;padding:24px 18px;max-width:1200px;overflow:hidden}:host .mfc-header__content .mfc-header__content__main{flex:25%;padding:0 15px 0 0;color:#fff;float:left}:host .mfc-header__content .mfc-header__content__main .mfc-header__content__main__link .mc-header__content__main__logo{cursor:pointer}:host .mfc-header__content .mfc-header__content__heading{display:flex;justify-content:center;flex-wrap:wrap;flex:25%;float:left;padding:0 15px 0 0;text-align:center}:host .mfc-header__content .mfc-header__content__heading .mfc-header__content__heading__element{flex:100%;font-size:25px;margin:0}:host .mfc-header__content .mfc-header__content__heading .mfc-header__content__heading__text{flex:100%;display:table-cell;font-size:18px;font-weight:500;padding:15px 0 0;margin:0;line-height:27px;vertical-align:middle}:host .mfc-header__content .mfc-header__content__contact{flex:auto;float:right;margin-top:-2px}:host .mfc-header__content .mfc-header__content__contact mat-icon{width:auto;height:auto}:host .mfc-header__content .mfc-header__content__contact .mfc-header__content__contact__phone .mfc-header__content__contact__phone__text{font-family:noto_sansregular,sans-serif;font-size:25px;font-weight:700;margin:0;text-align:right}:host .mfc-header__content .mfc-header__content__contact .mfc-header__content__contact__list{display:flex;justify-content:flex-end;margin:0;padding:15px 0 0;list-style:none}:host .mfc-header__content .mfc-header__content__contact .mfc-header__content__contact__list .mfc-header__content__contact__list__element{box-sizing:border-box;padding:0 8px;float:left}:host .mfc-header__content .mfc-header__content__contact .mfc-header__content__contact__list .mfc-header__content__contact__list__element:first-of-type{padding:0 8px 0 0}:host .mfc-header__content .mfc-header__content__contact .mfc-header__content__contact__list .mfc-header__content__contact__list__element:last-of-type{padding:0 0 0 8px}:host .mfc-header__content .mfc-header__content__contact .mfc-header__content__contact__list .mfc-header__content__contact__list__element .mfc-header__content__link{text-align:center}:host .mfc-header__content .mfc-header__content__contact .mfc-header__content__contact__list .mfc-header__content__contact__list__element .mfc-header__content__link .mfc-header__content__contact__list__text{display:inline;font-family:noto_sansregular,sans-serif;font-size:14px}@media (max-width:867px){:host .mfc-header__content{padding:5px}:host .mfc-header__content .mfc-header__content__main{display:flex;justify-content:center;flex:33%;padding:0}:host .mfc-header__content .mfc-header__content__main .mfc-header__content__main__link{padding:0 15px 0 0}:host .mfc-header__content .mfc-header__content__main .mfc-header__content__main__link .mc-header__content__main__logo{width:100%}:host .mfc-header__content .mfc-header__content__heading{flex:33%;padding:0}:host .mfc-header__content .mfc-header__content__heading .mfc-header__content__heading__element{font-size:14px}:host .mfc-header__content .mfc-header__content__heading .mfc-header__content__heading__text{padding:0;margin:0;font-size:12px}:host .mfc-header__content .mfc-header__content__contact{flex:33%;display:flex;justify-content:center;flex-wrap:wrap}:host .mfc-header__content .mfc-header__content__contact .mfc-header__content__contact__phone{flex:100%}:host .mfc-header__content .mfc-header__content__contact .mfc-header__content__contact__phone .mfc-header__content__contact__phone__text{font-size:13px;text-align:center}:host .mfc-header__content .mfc-header__content__contact .mfc-header__content__contact__list{display:none}}"]
            }] }
];
/** @nocollapse */
HeaderComponent.ctorParameters = () => [];
HeaderComponent.propDecorators = {
    logo_url: [{ type: Input }],
    logo_title: [{ type: Input }],
    loco_click: [{ type: Input }],
    title: [{ type: Input }],
    subtitle: [{ type: Input }],
    contact_phone: [{ type: Input }],
    icon_links: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    HeaderComponent.prototype.logo_url;
    /** @type {?} */
    HeaderComponent.prototype.logo_title;
    /** @type {?} */
    HeaderComponent.prototype.loco_click;
    /** @type {?} */
    HeaderComponent.prototype.title;
    /** @type {?} */
    HeaderComponent.prototype.subtitle;
    /** @type {?} */
    HeaderComponent.prototype.contact_phone;
    /** @type {?} */
    HeaderComponent.prototype.icon_links;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaGVhZGVyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BwY3JmL2NvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9oZWFkZXIvaGVhZGVyLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQ0EsT0FBTyxFQUFFLFNBQVMsRUFBVSxLQUFLLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFPekQsTUFBTSxPQUFPLGVBQWU7SUFZMUIsZ0JBQWUsQ0FBQzs7OztJQUVoQixRQUFRLEtBQUksQ0FBQzs7Ozs7SUFFYixZQUFZLENBQUMsS0FBSztRQUNoQixJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUM5QixDQUFDOzs7Ozs7SUFFRCxhQUFhLENBQUMsSUFBSSxFQUFFLEtBQUs7UUFDdkIsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDekIsQ0FBQzs7O1lBM0JGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsYUFBYTtnQkFDdkIsMm1EQUFzQzs7YUFFdkM7Ozs7O3VCQUVFLEtBQUs7eUJBQ0wsS0FBSzt5QkFDTCxLQUFLO29CQUVMLEtBQUs7dUJBQ0wsS0FBSzs0QkFFTCxLQUFLO3lCQUVMLEtBQUs7Ozs7SUFUTixtQ0FBMEI7O0lBQzFCLHFDQUE0Qjs7SUFDNUIscUNBQThCOztJQUU5QixnQ0FBdUI7O0lBQ3ZCLG1DQUEwQjs7SUFFMUIsd0NBQStCOztJQUUvQixxQ0FBZ0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJY29uTGluayB9IGZyb20gJy4vLi4vbW9kZWxzL2ljb24tbGluayc7XG5pbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAncGNyZi1oZWFkZXInLFxuICB0ZW1wbGF0ZVVybDogJy4vaGVhZGVyLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vaGVhZGVyLmNvbXBvbmVudC5zY3NzJ11cbn0pXG5leHBvcnQgY2xhc3MgSGVhZGVyQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgQElucHV0KCkgbG9nb191cmw6IHN0cmluZztcbiAgQElucHV0KCkgbG9nb190aXRsZTogc3RyaW5nO1xuICBASW5wdXQoKSBsb2NvX2NsaWNrOiBGdW5jdGlvbjtcblxuICBASW5wdXQoKSB0aXRsZTogc3RyaW5nO1xuICBASW5wdXQoKSBzdWJ0aXRsZTogc3RyaW5nO1xuXG4gIEBJbnB1dCgpIGNvbnRhY3RfcGhvbmU6IHN0cmluZztcblxuICBASW5wdXQoKSBpY29uX2xpbmtzOiBJY29uTGlua1tdO1xuXG4gIGNvbnN0cnVjdG9yKCkge31cblxuICBuZ09uSW5pdCgpIHt9XG5cbiAgbG9jb19jbGlja2VkKGV2ZW50KSB7XG4gICAgdGhpcy5sb2NvX2NsaWNrLmNhbGwoZXZlbnQpO1xuICB9XG5cbiAgb3Blbkljb25MaW5rcyhsaW5rLCBldmVudCkge1xuICAgIGxpbmsuY2xpY2suY2FsbChldmVudCk7XG4gIH1cbn1cbiJdfQ==