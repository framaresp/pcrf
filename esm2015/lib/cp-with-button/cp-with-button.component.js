/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input, Output, EventEmitter } from '@angular/core';
export class CpWithButtonComponent {
    constructor() {
        this.button_clicked = new EventEmitter();
        this.disabled = true;
    }
    /**
     * @return {?}
     */
    ngOnInit() { }
    /**
     * @param {?} event
     * @return {?}
     */
    postalCodeChecked(event) {
        console.log(`postalCodeChecked: ${event}`);
        this.disabled = !event;
        this.province = event;
    }
    /**
     * @param {?} event
     * @return {?}
     */
    buttonCLicked(event) {
        console.log(`buttonCLicked: ${event}`);
        this.button_clicked.emit(this.province);
    }
}
CpWithButtonComponent.decorators = [
    { type: Component, args: [{
                selector: 'pcrf-cp-with-button',
                template: "<pcrf-cp-input\n  [postal_code]=\"postal_code\"\n  [placeholder]=\"placeholder\"\n  (postal_code_info)=\"postalCodeChecked($event)\"\n></pcrf-cp-input>\n\n<pcrf-red-button\n  [disabled]=\"disabled\"\n  (event)=\"buttonCLicked($event)\"\n  [text]=\"text_button\"\n></pcrf-red-button>\n",
                styles: [""]
            }] }
];
/** @nocollapse */
CpWithButtonComponent.ctorParameters = () => [];
CpWithButtonComponent.propDecorators = {
    postal_code: [{ type: Input }],
    placeholder: [{ type: Input }],
    button_clicked: [{ type: Output }],
    text_button: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    CpWithButtonComponent.prototype.postal_code;
    /** @type {?} */
    CpWithButtonComponent.prototype.placeholder;
    /** @type {?} */
    CpWithButtonComponent.prototype.button_clicked;
    /** @type {?} */
    CpWithButtonComponent.prototype.text_button;
    /** @type {?} */
    CpWithButtonComponent.prototype.province;
    /** @type {?} */
    CpWithButtonComponent.prototype.disabled;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3Atd2l0aC1idXR0b24uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQHBjcmYvY29tbW9uLyIsInNvdXJjZXMiOlsibGliL2NwLXdpdGgtYnV0dG9uL2NwLXdpdGgtYnV0dG9uLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxLQUFLLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBRSxNQUFNLGVBQWUsQ0FBQztBQU8vRSxNQUFNLE9BQU8scUJBQXFCO0lBVWhDO1FBUFUsbUJBQWMsR0FBRyxJQUFJLFlBQVksRUFBVSxDQUFDO1FBS3RELGFBQVEsR0FBRyxJQUFJLENBQUM7SUFFRCxDQUFDOzs7O0lBRWhCLFFBQVEsS0FBSSxDQUFDOzs7OztJQUViLGlCQUFpQixDQUFDLEtBQUs7UUFDckIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxzQkFBc0IsS0FBSyxFQUFFLENBQUMsQ0FBQztRQUMzQyxJQUFJLENBQUMsUUFBUSxHQUFHLENBQUMsS0FBSyxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO0lBQ3hCLENBQUM7Ozs7O0lBRUQsYUFBYSxDQUFDLEtBQUs7UUFDakIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxrQkFBa0IsS0FBSyxFQUFFLENBQUMsQ0FBQztRQUN2QyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDMUMsQ0FBQzs7O1lBNUJGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUscUJBQXFCO2dCQUMvQix3U0FBOEM7O2FBRS9DOzs7OzswQkFFRSxLQUFLOzBCQUNMLEtBQUs7NkJBQ0wsTUFBTTswQkFDTixLQUFLOzs7O0lBSE4sNENBQTZCOztJQUM3Qiw0Q0FBNkI7O0lBQzdCLCtDQUFzRDs7SUFDdEQsNENBQTZCOztJQUU3Qix5Q0FBaUI7O0lBRWpCLHlDQUFnQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbnB1dCwgT3V0cHV0LCBFdmVudEVtaXR0ZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAncGNyZi1jcC13aXRoLWJ1dHRvbicsXG4gIHRlbXBsYXRlVXJsOiAnLi9jcC13aXRoLWJ1dHRvbi5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL2NwLXdpdGgtYnV0dG9uLmNvbXBvbmVudC5zY3NzJ11cbn0pXG5leHBvcnQgY2xhc3MgQ3BXaXRoQnV0dG9uQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgQElucHV0KCkgcG9zdGFsX2NvZGU6IHN0cmluZztcbiAgQElucHV0KCkgcGxhY2Vob2xkZXI6IHN0cmluZztcbiAgQE91dHB1dCgpIGJ1dHRvbl9jbGlja2VkID0gbmV3IEV2ZW50RW1pdHRlcjxzdHJpbmc+KCk7XG4gIEBJbnB1dCgpIHRleHRfYnV0dG9uOiBzdHJpbmc7XG5cbiAgcHJvdmluY2U6IHN0cmluZztcblxuICBkaXNhYmxlZCA9IHRydWU7XG5cbiAgY29uc3RydWN0b3IoKSB7fVxuXG4gIG5nT25Jbml0KCkge31cblxuICBwb3N0YWxDb2RlQ2hlY2tlZChldmVudCkge1xuICAgIGNvbnNvbGUubG9nKGBwb3N0YWxDb2RlQ2hlY2tlZDogJHtldmVudH1gKTtcbiAgICB0aGlzLmRpc2FibGVkID0gIWV2ZW50O1xuICAgIHRoaXMucHJvdmluY2UgPSBldmVudDtcbiAgfVxuXG4gIGJ1dHRvbkNMaWNrZWQoZXZlbnQpIHtcbiAgICBjb25zb2xlLmxvZyhgYnV0dG9uQ0xpY2tlZDogJHtldmVudH1gKTtcbiAgICB0aGlzLmJ1dHRvbl9jbGlja2VkLmVtaXQodGhpcy5wcm92aW5jZSk7XG4gIH1cbn1cbiJdfQ==