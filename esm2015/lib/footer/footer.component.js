/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input } from '@angular/core';
export class FooterComponent {
    constructor() { }
    /**
     * @return {?}
     */
    ngOnInit() { }
    /**
     * @param {?} link
     * @param {?} event
     * @return {?}
     */
    openIconLinks(link, event) {
        link.click.call(event);
    }
}
FooterComponent.decorators = [
    { type: Component, args: [{
                selector: 'pcrf-footer',
                template: "<div class=\"mfc-footer__main\">\r\n  <div class=\"mfc-footer__main-upper\">\r\n    <ul class=\"mfc-footer__main--list-links\">\r\n      <li\r\n        *ngFor=\"let link of left_links\"\r\n        class=\"mfc-footer__main--list-links__element\"\r\n      >\r\n        <a class=\"mfc-footer__link\" (click)=\"openIconLinks(link, $event)\">\r\n          {{ link.label }}\r\n        </a>\r\n        <div class=\"mfc-u-div-inline\">|</div>\r\n      </li>\r\n    </ul>\r\n  </div>\r\n  <ul class=\"mfc-footer__main--list-icon-links\">\r\n    <li *ngFor=\"let link of right_links\" class=\"mfc-footer__main__link\">\r\n      <a\r\n        [title]=\"link.label\"\r\n        class=\"mfc-footer__link\"\r\n        (click)=\"openIconLinks(link, $event)\"\r\n      >\r\n        <mat-icon\r\n          fontSet=\"fa\"\r\n          [fontIcon]=\"link.icon\"\r\n          class=\"mfc-icon\"\r\n        ></mat-icon>\r\n        {{ link.label }}\r\n      </a>\r\n    </li>\r\n  </ul>\r\n  <div class=\"mfc-footer__separator\"></div>\r\n  <div class=\"mfc-footer__copyright\">\r\n    <p ng-bind-html=\"copyright\" class=\"mfc-footer__copyright--no-text-link\">\r\n      {{ footer_text }}\r\n    </p>\r\n  </div>\r\n</div>\r\n",
                styles: [":host{background-color:#d81e05;display:block;padding:0 12px;overflow:hidden}:host .mfc-footer__main{margin:0 auto;max-width:1200px;padding:20px 0}:host .mfc-footer__main .mfc-footer__main--list-icon-links{box-sizing:border-box;float:right;margin-top:-5px;text-align:right}:host .mfc-footer__main .mfc-footer__main--list-icon-links .mfc-footer__main__link{box-sizing:border-box;display:inline;margin:0 0 0 25px}:host .mfc-footer__main .mfc-footer__main--list-icon-links .mfc-footer__main__link .mfc-footer__link{background-color:rgba(0,0,0,0);color:#fff;font-family:noto_sansregular,sans-serif;-moz-text-size-adjust:100%;-ms-text-size-adjust:100%;cursor:pointer}:host .mfc-footer__main .mfc-footer__main-upper{float:left;overflow:hidden;margin:-5px 0 0}:host .mfc-footer__main .mfc-footer__main-upper .mfc-footer__main--list-links{margin:0;padding:0}:host .mfc-footer__main .mfc-footer__main-upper .mfc-footer__main--list-links .mfc-footer__main--list-links__element{list-style:none;float:left}:host .mfc-footer__main .mfc-footer__main-upper .mfc-footer__main--list-links .mfc-footer__main--list-links__element:last-of-type .mfc-u-div-inline{display:none}:host .mfc-footer__main .mfc-footer__main-upper .mfc-footer__main--list-links .mfc-footer__main--list-links__element .mfc-footer__link{color:#fff;font-family:noto_sansregular,sans-serif;font-size:14px;font-weight:600;text-transform:uppercase;cursor:pointer}:host .mfc-footer__main .mfc-footer__main-upper .mfc-footer__main--list-links .mfc-footer__main--list-links__element .mfc-u-div-inline{color:#fff;display:inline;padding:0 5px;font-weight:600}:host .mfc-footer__main .mfc-footer__separator{clear:both;height:1px;background:linear-gradient(to right,rgba(255,255,255,0) 0,#fff 10%,#fff 90%,rgba(255,255,255,0) 100%)}:host .mfc-footer__main .mfc-footer__copyright{clear:both;float:left;display:block;width:100%}:host .mfc-footer__main .mfc-footer__copyright .mfc-footer__copyright--no-text-link{display:flex;justify-content:center;color:#fff;font-size:13px}@media (max-width:867px){:host .mfc-footer__main .mfc-footer__main-upper{display:flex;justify-content:center;width:100%}:host .mfc-footer__main .mfc-footer__main--list-icon-links{display:flex;flex-wrap:wrap;width:100%;padding:10px 0 5px;margin:0}:host .mfc-footer__main .mfc-footer__main--list-icon-links .mfc-footer__main__link{flex:auto;margin:0;text-align:center}}"]
            }] }
];
/** @nocollapse */
FooterComponent.ctorParameters = () => [];
FooterComponent.propDecorators = {
    left_links: [{ type: Input }],
    right_links: [{ type: Input }],
    footer_text: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    FooterComponent.prototype.left_links;
    /** @type {?} */
    FooterComponent.prototype.right_links;
    /** @type {?} */
    FooterComponent.prototype.footer_text;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9vdGVyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BwY3JmL2NvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9mb290ZXIvZm9vdGVyLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxLQUFLLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFRekQsTUFBTSxPQUFPLGVBQWU7SUFNMUIsZ0JBQWUsQ0FBQzs7OztJQUVoQixRQUFRLEtBQUksQ0FBQzs7Ozs7O0lBRWIsYUFBYSxDQUFDLElBQUksRUFBRSxLQUFLO1FBQ3ZCLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3pCLENBQUM7OztZQWpCRixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLGFBQWE7Z0JBQ3ZCLGdzQ0FBc0M7O2FBRXZDOzs7Ozt5QkFFRSxLQUFLOzBCQUNMLEtBQUs7MEJBRUwsS0FBSzs7OztJQUhOLHFDQUFnQzs7SUFDaEMsc0NBQWlDOztJQUVqQyxzQ0FBNkIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEljb25MaW5rIH0gZnJvbSAnLi4vbW9kZWxzL2ljb24tbGluayc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ3BjcmYtZm9vdGVyJyxcbiAgdGVtcGxhdGVVcmw6ICcuL2Zvb3Rlci5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL2Zvb3Rlci5jb21wb25lbnQuc2NzcyddXG59KVxuZXhwb3J0IGNsYXNzIEZvb3RlckNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gIEBJbnB1dCgpIGxlZnRfbGlua3M6IEljb25MaW5rW107XG4gIEBJbnB1dCgpIHJpZ2h0X2xpbmtzOiBJY29uTGlua1tdO1xuXG4gIEBJbnB1dCgpIGZvb3Rlcl90ZXh0OiBzdHJpbmc7XG5cbiAgY29uc3RydWN0b3IoKSB7fVxuXG4gIG5nT25Jbml0KCkge31cblxuICBvcGVuSWNvbkxpbmtzKGxpbmssIGV2ZW50KSB7XG4gICAgbGluay5jbGljay5jYWxsKGV2ZW50KTtcbiAgfVxufVxuIl19