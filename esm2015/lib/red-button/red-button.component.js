/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input, EventEmitter, Output } from '@angular/core';
export class RedButtonComponent {
    constructor() {
        this.event = new EventEmitter();
    }
    /**
     * @return {?}
     */
    ngOnInit() { }
    /**
     * @param {?} event
     * @return {?}
     */
    action(event) {
        this.event.emit(event);
    }
}
RedButtonComponent.decorators = [
    { type: Component, args: [{
                selector: 'pcrf-red-button',
                template: "<button\n  mat-raised-button\n  color=\"warn\"\n  [disabled]=\"disabled\"\n  (click)=\"action($event)\"\n>\n  {{ text }}\n</button>\n",
                styles: [""]
            }] }
];
/** @nocollapse */
RedButtonComponent.ctorParameters = () => [];
RedButtonComponent.propDecorators = {
    disabled: [{ type: Input }],
    event: [{ type: Output }],
    text: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    RedButtonComponent.prototype.disabled;
    /** @type {?} */
    RedButtonComponent.prototype.event;
    /** @type {?} */
    RedButtonComponent.prototype.text;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVkLWJ1dHRvbi5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AcGNyZi9jb21tb24vIiwic291cmNlcyI6WyJsaWIvcmVkLWJ1dHRvbi9yZWQtYnV0dG9uLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxLQUFLLEVBQUUsWUFBWSxFQUFFLE1BQU0sRUFBRSxNQUFNLGVBQWUsQ0FBQztBQU8vRSxNQUFNLE9BQU8sa0JBQWtCO0lBSzdCO1FBSFUsVUFBSyxHQUFHLElBQUksWUFBWSxFQUFXLENBQUM7SUFHL0IsQ0FBQzs7OztJQUVoQixRQUFRLEtBQUksQ0FBQzs7Ozs7SUFFYixNQUFNLENBQUMsS0FBSztRQUNWLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3pCLENBQUM7OztZQWhCRixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLGlCQUFpQjtnQkFDM0IsaUpBQTBDOzthQUUzQzs7Ozs7dUJBRUUsS0FBSztvQkFDTCxNQUFNO21CQUNOLEtBQUs7Ozs7SUFGTixzQ0FBMkI7O0lBQzNCLG1DQUE4Qzs7SUFDOUMsa0NBQXNCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIElucHV0LCBFdmVudEVtaXR0ZXIsIE91dHB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdwY3JmLXJlZC1idXR0b24nLFxuICB0ZW1wbGF0ZVVybDogJy4vcmVkLWJ1dHRvbi5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL3JlZC1idXR0b24uY29tcG9uZW50LnNjc3MnXVxufSlcbmV4cG9ydCBjbGFzcyBSZWRCdXR0b25Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICBASW5wdXQoKSBkaXNhYmxlZDogQm9vbGVhbjtcbiAgQE91dHB1dCgpIGV2ZW50ID0gbmV3IEV2ZW50RW1pdHRlcjxib29sZWFuPigpO1xuICBASW5wdXQoKSB0ZXh0OiBzdHJpbmc7XG5cbiAgY29uc3RydWN0b3IoKSB7fVxuXG4gIG5nT25Jbml0KCkge31cblxuICBhY3Rpb24oZXZlbnQpIHtcbiAgICB0aGlzLmV2ZW50LmVtaXQoZXZlbnQpO1xuICB9XG59XG4iXX0=