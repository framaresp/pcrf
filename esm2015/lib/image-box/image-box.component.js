/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input } from '@angular/core';
export class ImageBoxComponent {
    constructor() { }
    /**
     * @return {?}
     */
    ngOnInit() { }
}
ImageBoxComponent.decorators = [
    { type: Component, args: [{
                selector: 'pcrf-image-box',
                template: "<mat-card class=\"example-card\">\n  <mat-card-header>\n    <mat-card-title>{{ title }}</mat-card-title>\n    <mat-card-subtitle>{{ subtitle }}</mat-card-subtitle>\n  </mat-card-header>\n  <img mat-card-image [src]=\"image_url\" alt=\"photo\" />\n</mat-card>\n",
                styles: [""]
            }] }
];
/** @nocollapse */
ImageBoxComponent.ctorParameters = () => [];
ImageBoxComponent.propDecorators = {
    title: [{ type: Input }],
    subtitle: [{ type: Input }],
    image_url: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    ImageBoxComponent.prototype.title;
    /** @type {?} */
    ImageBoxComponent.prototype.subtitle;
    /** @type {?} */
    ImageBoxComponent.prototype.image_url;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW1hZ2UtYm94LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BwY3JmL2NvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9pbWFnZS1ib3gvaW1hZ2UtYm94LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxLQUFLLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFPekQsTUFBTSxPQUFPLGlCQUFpQjtJQUs1QixnQkFBZSxDQUFDOzs7O0lBRWhCLFFBQVEsS0FBSSxDQUFDOzs7WUFaZCxTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLGdCQUFnQjtnQkFDMUIsZ1JBQXlDOzthQUUxQzs7Ozs7b0JBRUUsS0FBSzt1QkFDTCxLQUFLO3dCQUNMLEtBQUs7Ozs7SUFGTixrQ0FBdUI7O0lBQ3ZCLHFDQUEwQjs7SUFDMUIsc0NBQTJCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ3BjcmYtaW1hZ2UtYm94JyxcbiAgdGVtcGxhdGVVcmw6ICcuL2ltYWdlLWJveC5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL2ltYWdlLWJveC5jb21wb25lbnQuc2NzcyddXG59KVxuZXhwb3J0IGNsYXNzIEltYWdlQm94Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgQElucHV0KCkgdGl0bGU6IHN0cmluZztcbiAgQElucHV0KCkgc3VidGl0bGU6IHN0cmluZztcbiAgQElucHV0KCkgaW1hZ2VfdXJsOiBzdHJpbmc7XG5cbiAgY29uc3RydWN0b3IoKSB7fVxuXG4gIG5nT25Jbml0KCkge31cbn1cbiJdfQ==